﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.CadastrarFabricantes;
using Tela_principal.Cadastro_Clientes.Tela;
using Tela_principal.Cadastro_de_veiculos;
using Tela_principal.Cadastro_Funcionario.Tela;
using Tela_principal.Cadastro_Usuario.Perfil_de_Usuario;
using Tela_principal.Orçamento;
using Tela_principal.Sub_Telas;
using Tela_principal.Telas;
using Tela_principal.Telas.Clientes;
using Tela_principal.Telas.Em_processo.Usuário.Cadastro;
using Tela_principal.Telas.Funcionários;

namespace Tela_principal
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmConsultar_Usuario());

        }
    }
}
