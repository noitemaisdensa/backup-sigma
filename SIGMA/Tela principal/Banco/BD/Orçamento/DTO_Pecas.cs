﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Orcamento
{
    class DTO_Pecas
    {
        public int ID { get; set; }
        public int ID_Veiculo { get; set; }
        public int ID_Fabricante { get; set; }
        public int ID_Fornecedor { get; set; }
        public int ID_Atendimento { get; set; }
        public double Valor { get; set; }
        public int Qtnd { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
    }
}
