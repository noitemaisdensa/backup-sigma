﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.DB;
using Tela_principal.Fim_Orcamento;
using Tela_principal.Orcamento;

namespace Tela_principal.Orçamento
{
    class Database_Orcamento
    {
        

        public List<VIEW_Fim_orcamento> Listar()
        {
            string script = @"SELECT * FROM vw_fimorcamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<VIEW_Fim_orcamento> lista = new List<VIEW_Fim_orcamento>();
            while (reader.Read())
            {
                VIEW_Fim_orcamento dto = new VIEW_Fim_orcamento();
                dto.ID = reader.GetInt32("id_veiculo");
                dto.Nome = reader.GetString("nm_nome");
                dto.Placa = reader.GetString("ds_placa");
                dto.Ano = reader.GetDateTime("dt_ano");
                dto.Modelo = reader.GetString("ds_modelo");
                dto.Valor = reader.GetDouble("vl_valor");
                dto.Valor_Total = reader.GetDouble("vl_total");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<VIEW_Servico> Servico()
        {

            string script = "SELECT * FROM vw_servico";

            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_Servico> oi = new List<VIEW_Servico>();

            while (reader.Read())
            {
                VIEW_Servico dto = new VIEW_Servico();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Modelo = reader.GetString("ds_modelo");
                dto.Preco = reader.GetDouble("nr_preco_servico");
                dto.Servico = reader.GetString("ds_servico");
                dto.Placa = reader.GetString("ds_placa");
                            
                oi.Add(dto);
            }

            reader.Close();


            return oi;
        }
        
        

        public List<DTO_Pecas> Pecas()
        {
            string script = @"SELECT * FROM tb_pecas";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Pecas> lista = new List<DTO_Pecas>();

            while (reader.Read())
           {
                DTO_Pecas dto = new DTO_Pecas();
                dto.ID = reader.GetInt32("id_pecas");
                dto.Nome = reader.GetString("nm_nome");
                dto.Qtnd = reader.GetInt32("nr_qtnd");
                dto.Valor = reader.GetDouble("nr_valor");
                dto.Descricao = reader.GetString("ds_descricao");

                lista.Add(dto);
           }
            reader.Close();
            return lista;

        }

        

       





    }
}
