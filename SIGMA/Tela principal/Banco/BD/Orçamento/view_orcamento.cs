﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Orçamento
{
    class VIEW_Orcamento
    {
        public int ID { get; set; }
        public string Observacao { get; set; }
        public string Placa { get; set; }
        public double Valor { get; set; }
        public string Descricao { get; set; }
        public DateTime Data_Entrega { get; set; }
        public DateTime Data_Saida { get; set; }
        public string Fun_Nome { get; set; }

    }


  
}
