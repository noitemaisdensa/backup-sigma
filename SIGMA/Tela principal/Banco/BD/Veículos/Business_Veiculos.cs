﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Cadastro_de_veiculos
{
    public class Business_Veiculos
    {
        public int Salvar(DTO_Veiculos dto)
        {
            if (dto.Observacoes.Trim() == string.Empty)
            {
                throw new ArgumentException("Observações são obrigatórias!");
            }
            if (dto.Combustivel.Trim() == string.Empty)
            {
                throw new ArgumentException("Combustível é obrigatório!");
            }
            if (dto.Odometro == 0)
            {
                throw new ArgumentException("Odometro é obrigatório!");
            }
            if (dto.Cor.Trim() == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório!");
            }
            if (dto.Placa.Trim() == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório!");
            }
            Validacao validar = new Validacao();
            bool v = validar.Placa(dto.Placa);
            if (v == false)
            {
                throw new ArgumentException("Placa incorreta!" + "\n" + "Veja se não digitou algo errado!");
            }
            Database_Veiculos db = new Database_Veiculos();
            bool placa = db.VerificarPlaca(dto);
            if (placa == true)
            {
                throw new ArgumentException("Este veículo já consta em nossa base de dados");
            }
            return db.Salvar(dto);

        }
        public void Alterar (DTO_Veiculos dto)
        {
            if (dto.Observacoes.Trim() == string.Empty)
            {
                throw new ArgumentException("Observações são obrigatórias!");
            }
            if (dto.Combustivel.Trim() == string.Empty)
            {
                throw new ArgumentException("Combustível é obrigatório!");
            }
            if (dto.Odometro == 0)
            {
                throw new ArgumentException("Odometro é obrigatório!");
            }
            if (dto.Cor.Trim() == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório!");
            }
            if (dto.Placa.Trim() == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório!");
            }
            Validacao validar = new Validacao();
            bool v = validar.Placa(dto.Placa);
            if (v == false)
            {
                throw new ArgumentException("Placa incorreta!" + "\n" + "Veja se não digitou algo errado!");
            }
            Database_Veiculos db = new Database_Veiculos();
            db.Alterar(dto);
        }
        public void Remover (int id)
        {
            Database_Veiculos db = new Database_Veiculos();
            db.Remover(id);
        }
        public List<DTO_Veiculos> Listar ()
        {
            Database_Veiculos db = new Database_Veiculos();
            List<DTO_Veiculos> list = db.Listar();

            return list;
        }

        public List<DTO_Veiculos> Consultar(DTO_Veiculos dto)
        {
            Database_Veiculos db = new Database_Veiculos();
            List<DTO_Veiculos> list = db.Consultar(dto);

            return list;
        }
    }
}
