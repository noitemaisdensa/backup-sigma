﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Ordem_de_Servico
{
    class Database_Ordem_Servico
    {
        //Salvar 
        public List<DTO_Ordem_Servico> Consultar()
        {
            string script = @"SELECT * FROM vw_fimorcamento  ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Ordem_Servico> lista = new List<DTO_Ordem_Servico>();
            while (reader.Read())
            {
                DTO_Ordem_Servico dto = new DTO_Ordem_Servico();
                dto.ID = reader.GetInt32("id_veiculo");
                dto.placa = reader.GetString("ds_placa");
                dto.Observacoes = reader.GetString("ds_observacao");
                dto.Valor = reader.GetDouble("vl_valor");
                dto.Funcionario = reader.GetString("nm_nome");
                dto.DataDeEntrega = reader.GetDateTime("dt_data_de_entrega");
                dto.DataDesaida = reader.GetDateTime("dt_data_de_saida");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Ordem_Servico> ConsultarFiltro(DateTime inicio, DateTime fim )
        {
            string script = @"SELECT * FROM vw_fimorcamento 
                                WHERE dt_data_de_entrega like @dt_data_de_entrega 
                                                      or 
                                       dt_data_de_saida like dt_data_de_saida ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_data_de_entrega","%" + inicio + "%"));
            parms.Add(new MySqlParameter("dt_data_de_saida", "%" +  fim + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Ordem_Servico> lista = new List<DTO_Ordem_Servico>();
            while (reader.Read())
            {
                DTO_Ordem_Servico dto = new DTO_Ordem_Servico();
                dto.ID = reader.GetInt32("id_veiculo");
                dto.placa = reader.GetString("ds_placa");
                dto.Observacoes = reader.GetString("ds_observacao");
                dto.Valor = reader.GetDouble("vl_valor");
                dto.Funcionario = reader.GetString("nm_nome");
                dto.DataDeEntrega = reader.GetDateTime("dt_data_de_entrega");
                dto.DataDesaida = reader.GetDateTime("dt_data_de_saida");


                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }
    }
}
