﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_Clientes
{
    public class Business_Cliente
    {
        public int Salvar(DTO_Cliente dto)
        {

            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }

            if (dto.Profissao.Trim() == string.Empty)
            {
                throw new ArgumentException("Profissão é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }

            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório!");
            }
            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Endereco.Trim() == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }
            if (dto.Telefone_Residencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Tipo_Carta == string.Empty)
            {
                throw new ArgumentException("Tipo de carta é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório!");
            }
            if (dto.CNPJ == "  .   .   /    -")
            {
                dto.CNPJ = null;
            }
            if (dto.CPF == "   .   .   -")
            {
                dto.CPF = null;
            }
            if (dto.RG == "  .   .   -")
            {
                dto.RG = null;
            }
            Database_Cliente db = new Database_Cliente();
            bool email = db.VerificarEmail(dto);
            if (email == true)
            {
                throw new ArgumentException("Este EMAIL já consta em nossa base de dados!");
            }
            bool contatos = db.VerificarContato(dto);
            if (contatos == true)
            {
                throw new ArgumentException("Esses contatos (Telefone, celular ou Residencial) já constam em nossa base de dados!");
            }
            bool clientes = db.VerificarClientes(dto);
            if (clientes == true)
            {
                throw new ArgumentException("Esse cliente já consta em nossa base de dados!");
            }
            return db.Salvar(dto);
        }

        public void Alterar(DTO_Cliente dto)
        {
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }

            if (dto.Profissao.Trim() == string.Empty)
            {
                throw new ArgumentException("Profissão é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }

            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório!");
            }
            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("RG é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Endereco.Trim() == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!" + "\n" + "Não é possível cadastrar valores em vázio.");
            }
            if (dto.Telefone_Residencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Tipo_Carta == string.Empty)
            {
                throw new ArgumentException("Tipo de carta é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório!");
            }
            if (dto.CNPJ == "  .   .   /    -")
            {
                dto.CNPJ = null;
            }
            if (dto.CPF == "   .   .   -")
            {
                dto.CPF = null;
            }
            if (dto.RG == "  .   .   -")
            {
                dto.RG = null;
            }

            Database_Cliente db = new Database_Cliente();
            db.Alterar(dto);
        }

        public void Remover(int dto)
        {
            Database_Cliente db = new Database_Cliente();
            db.Remover(dto);
        }
        public List<DTO_Cliente> Consultar(DTO_Cliente dto)
        {
            Database_Cliente db = new Database_Cliente();
            List<DTO_Cliente> lista = db.Consultar(dto);

            return lista;
        }
        public List<DTO_Cliente> ConsultarLabel(int dto)
        {
            Database_Cliente db = new Database_Cliente();
            List<DTO_Cliente> lista = db.ConsultarLabel(dto);

            return lista;
        }
        public List<DTO_Cliente> Listar()
        {
            Database_Cliente db = new Database_Cliente();
            List<DTO_Cliente> lista = db.Listar();

            return lista;
        }

    }
}
