﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_de_Clientes
{
    public class DTO_Cliente
    {
        public int ID { get; set; } //ok
        public string CPF { get; set; }//ok
        public string CNPJ { get; set; }//ok
        public string RG { get; set; }//ok
        public string Nome { get; set; } //ok
        public DateTime DatadeNascimento { get; set; }//ok
        public string CEP { get; set; }//ok
        public string UF { get; set; }
        public string Endereco { get; set; }//ok
        public string Telefone_Residencial { get; set; }//ok
        public string Telefone { get; set; }//ok
        public string Celular { get; set; }//ok
        public string Profissao { get; set; }//ok
        public string Tipo_Carta { get; set; }
        public DateTime ClienteDesde { get; set; }//ok
        public string Email { get; set; }//ok
        public bool Sexo { get; set; }//ok
        public string Complemento { get; set; }//ok
        
        public decimal Numero { get; set; }
      /* Se o cadastro de clientes é somente para pessoa fisica precisa do CNPJ ?
      * public string cnpj { get; set; }//ok */
        
        //UF é de outra tabela
    }
}
