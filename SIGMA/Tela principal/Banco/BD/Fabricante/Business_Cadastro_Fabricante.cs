﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.CadastrarFabricantes
{
    public class Business_Cadastro_Fabricante
    {
        Validacao validar = new Validacao();
        public int Salvar(DTO_Cadastrar_Fabricantes dto)
        {
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.CNPJ.Trim() == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.Descricao.Trim() == string.Empty)
            {
                throw new ArgumentException("Descricão é obrigatório!");
            }
            if (dto.CNPJ == "  .   .   /    -")
            {
                throw new ArgumentException("CNPJ não pode estar vázio");
            }
            if (validar.VerificarCNPJ(dto.CNPJ) == false)
            {
                throw new ArgumentException("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta");
            }

            Database_Cadastrar_Fabricante db = new Database_Cadastrar_Fabricante();
            bool tem = db.VerificarFabricantes(dto);
            if (tem == true)
            {
                throw new ArgumentException("Esse Fabricante já consta em nossa base de dados!");
            }
            return db.Salvar(dto);
        }
        public void Remover(int dto)
        {
            Database_Cadastrar_Fabricante db = new Database_Cadastrar_Fabricante();
            db.Remover(dto);
        }
        public List<DTO_Cadastrar_Fabricantes> Consultar(DTO_Cadastrar_Fabricantes dto)
        {
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }

            Database_Cadastrar_Fabricante db = new Database_Cadastrar_Fabricante();
            List<DTO_Cadastrar_Fabricantes> list = db.Consultar(dto);

            return list;
        }
        public void Alterar(DTO_Cadastrar_Fabricantes dto)
        {
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.CNPJ.Trim() == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório!");
            }
            if (dto.Descricao.Trim() == string.Empty)
            {
                throw new ArgumentException("Descricão é obrigatório!");
            }
            if (dto.CNPJ == "  .   .   /    -")
            {
                throw new ArgumentException("CNPJ não pode estar vázio");
            }
            if (validar.VerificarCNPJ(dto.CNPJ) == false)
            {
                throw new ArgumentException("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta");
            }

            Database_Cadastrar_Fabricante db = new Database_Cadastrar_Fabricante();
            db.Alterar(dto);
        }
    }
}
