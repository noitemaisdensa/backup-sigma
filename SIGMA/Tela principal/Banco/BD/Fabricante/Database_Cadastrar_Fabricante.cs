﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.CadastrarFabricantes
{
    public class Database_Cadastrar_Fabricante
    {
        public int Salvar (DTO_Cadastrar_Fabricantes dto)
        {
            string script = @"INSERT INTO tb_fabricante
                              (nm_nome,ds_cnpj,ds_descricao)
                                VALUES
                                  (@nm_nome,@ds_cnpj,@ds_descricao) ";
            //Mas que montagem feia

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome",dto.Nome));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_descricao", dto.Descricao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Remover (int dto)
        {
            string script = @"DELETE FROM tb_fabricante
                              WHERE id_fabricante = @id_fabricante";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fabricante", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public List<DTO_Cadastrar_Fabricantes> Consultar (DTO_Cadastrar_Fabricantes dto)
        {
            string script = @"SELECT * FROM tb_fabricante
                            WHERE nm_nome LIKE @nm_nome";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Cadastrar_Fabricantes> fora = new List<DTO_Cadastrar_Fabricantes>();

            while (reader.Read())
            {
                DTO_Cadastrar_Fabricantes dentro = new DTO_Cadastrar_Fabricantes();
                dentro.ID = reader.GetInt32("id_fabricante");
                dentro.Nome = reader.GetString("nm_nome");
                dentro.Descricao = reader.GetString("ds_descricao");
                dentro.CNPJ = reader.GetString("ds_cnpj");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
        public void Alterar (DTO_Cadastrar_Fabricantes dto)
        {
            string script =
                @"UPDATE tb_fabricante
                     SET nm_nome       = @nm_nome,
                         ds_descricao  = @ds_descricao,
                         ds_cnpj       = @ds_cnpj

                  WHERE id_fabricante  = @id_fabricante";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fabricante", dto.ID));
            parm.Add(new MySqlParameter("nm_nome", dto.Nome));
            parm.Add(new MySqlParameter("ds_descricao", dto.Descricao));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public bool VerificarFabricantes (DTO_Cadastrar_Fabricantes dto)
        {
            string script = "SELECT * FROM tb_fabricante" +
                "                    WHERE ds_cnpj = @ds_cnpj";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
