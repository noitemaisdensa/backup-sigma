﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tela_principal.Funções.Apenas_Letra;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Cadastro_de_funcionario
{
    public class Business_Funcionario
    {
        Validacao validar = new Validacao();
        Caracter_Digito cd = new Caracter_Digito();

        public int Salvar(DTO_Funcionario dto)
        {
            cd.contemLetras(dto.Nome);

            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Endereco.Trim() == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número da casa é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.TelefoneResidencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Cidade.Trim() == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório!");
            }
            if (validar.VerificarCPF(dto.CPF) == false)
            {
                throw new ArgumentException("CPF inválido!" + "\n" + "Verifique se o CPF está digitalizado da maneira correta");
            }
            if (dto.Foto.Trim() == string.Empty)
            {
                throw new ArgumentException("Foto é obrigatória!");
            }
            Database_Funcionario db = new Database_Funcionario();
            bool email = db.VerificarEmail(dto);
            if (email == true)
            {
                throw new ArgumentException("Este EMAIL já consta em nossa base de dados!");
            }
            bool contatos = db.VerificarContatos(dto);
            if (contatos == true)
            {
                throw new ArgumentException("Esses contatos (Telefone, celular ou Residencial) já constam em nossa base de dados!");
            }
            bool funcionario = db.VerificarFuncionario(dto);
            if (funcionario == true)
            {
                throw new ArgumentException("Este funcionário já consta em nossa base de dados");
            }
            return db.Salvar(dto);

        }
        public void Alterar(DTO_Funcionario dto)
        {
            cd.contemLetras(dto.Nome);

            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.DatadeNascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatório!");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Endereco.Trim() == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório!");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório!");
            }
            if (dto.UF == string.Empty)
            {
                throw new ArgumentException("UF é obrigatório!");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número da casa é obrigatório!");
            }
            string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
            Regex regex = new Regex(re);
            if (regex.IsMatch(dto.Email) == false)
            {
                throw new ArgumentException("Email inválido!");
            }
            if (dto.TelefoneResidencial == string.Empty)
            {
                throw new ArgumentException("Telefone residencial é obrigatório!");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório!");
            }
            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Celular é obrigatório!");
            }
            if (dto.Cidade.Trim() == string.Empty)
            {
                throw new ArgumentException("Cidade é obrigatório!");
            }

            Database_Funcionario db = new Database_Funcionario();
            db.Alterar(dto);
        }
        public void Remover (int dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            db.Remover(dto);
        }
        public List<DTO_Funcionario> Listar()
        {
            Database_Funcionario db = new Database_Funcionario();
            List<DTO_Funcionario> lista = db.Listar();

            return lista;
        }
        public List<DTO_Funcionario> Consultar (DTO_Funcionario dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            List<DTO_Funcionario> consult = db.Consultar(dto);

            return consult;
        }
    }
}
