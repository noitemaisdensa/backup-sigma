﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_Usuario
{
    public class Database_Usuario
    {
        public int Salvar(DTO_Usuarios dto)
        {
            string script =
            @"INSERT INTO tb_usuario
            ( 
                id_departamento,
                nm_nome, 
                nm_usuario,
                ds_senha,
                ds_checar,
                ds_perfil,
                ds_observacao
            )
            VALUES
            (
                @id_departamento,
                @nm_nome, 
                @nm_usuario,
                @ds_senha,
                @ds_checar,
                @ds_perfil,
                @ds_observacao
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome_User));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_checar", dto.Checar));
            parms.Add(new MySqlParameter("ds_perfil", dto.Perfil));
            parms.Add(new MySqlParameter("ds_observacao", dto.Obeservacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
            }

        public DTO_Usuarios Logar(DTO_Usuarios dto)
        {
            string script =
                @"SELECT * FROM tb_usuario
                   WHERE nm_usuario = @nm_usuario
                     AND ds_senha   = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            DTO_Usuarios dentro = null;
            if (reader.Read())
            {
                dentro = new DTO_Usuarios();
                dentro.Usuario = reader.GetString("nm_usuario");
                dentro.Senha = reader.GetString("ds_senha");

                dentro.Checar = reader.GetBoolean("ds_checar");
            }
            reader.Close();
            return dentro;

       }
        public bool VerificarUsuario (DTO_Usuarios dto)
        {
            string script = @"SELECT * FROM tb_usuario
                                      WHERE nm_usuario = @nm_usuario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;   
            }
        }

        public void Alterar(DTO_Usuarios dto)
        {
            string script =
               @"UPDATE tb_usuario 
                    SET id_departamento     = @id_departamento,
                        nm_nome             = @nm_nome, 
                        nm_usuario          = @nm_usuario,
                        ds_senha            = @ds_senha,
                        ds_checar           = @ds_checar,
                        ds_observacao       = @ds_observacao
                  WHERE id_usuario          = @id_usuario";

            //alterar diferentemente do salvar ele prescisa do ID

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_usuario", dto.ID));
            parms.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome_User));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("ds_checar", dto.Checar));
            parms.Add(new MySqlParameter("ds_observacao", dto.Obeservacao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script = @"DELETE  from tb_usuario Where id_usuario = @id_usuario";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_usuario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public List<DTO_Usuarios> Listar ()
        {
            string script = @"SELECT * FROM tb_usuario";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Usuarios> fora = new List<DTO_Usuarios>();

            while (reader.Read())
            {
                DTO_Usuarios dentro = new DTO_Usuarios();
                dentro.ID = reader.GetInt32("id_usuario");
                dentro.ID_Departamento = reader.GetInt32("id_departamento");
                dentro.Nome_User = reader.GetString("nm_nome");
                dentro.Usuario = reader.GetString("nm_usuario");
                dentro.Senha = reader.GetString("ds_senha");
                dentro.Checar = reader.GetBoolean("ds_checar");
                dentro.Perfil = reader.GetString("ds_perfil");
                dentro.Obeservacao = reader.GetString("ds_observacao");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;

        }
        public List<DTO_Usuarios> Consultar(DTO_Usuarios dto)
        {
            string script = @"SELECT * FROM tb_usuario
                                      WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", "%" + dto.Nome_User + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Usuarios> fora = new List<DTO_Usuarios>();

            while (reader.Read())
            {
                DTO_Usuarios dentro = new DTO_Usuarios();
                dentro.ID = reader.GetInt32("id_usuario");
                dentro.ID_Departamento = reader.GetInt32("id_departamento");
                dentro.Nome_User = reader.GetString("nm_nome");
                dentro.Usuario = reader.GetString("nm_usuario");
                dentro.Senha = reader.GetString("ds_senha");
                dentro.Checar = reader.GetBoolean("ds_checar");
                dentro.Perfil = reader.GetString("ds_perfil");
                dentro.Obeservacao = reader.GetString("ds_observacao");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
        public bool VerificarSenha (DTO_Usuarios dto)
        {
            string script = @"SELECT * FROM tb_usuario
                                       WHERE ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
