﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_Usuario
{
    public class DTO_Usuarios
    {
        public int ID { get; set; } //OK
        public int ID_Departamento { get; set; } //OK
        public string Nome_User { get; set; } //OK
        public string Usuario { get; set; } //OK
        public string Senha { get; set; } //OK
        public string Perfil { get; set; } //OK
        public bool Checar { get; set; } //Autorizar acesso  //OK
        public string Obeservacao { get; set; }//OK
    }
}
