﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Cadastro_Usuario
{
    public class Business_Usuario
    {
        Validacao validar = new Validacao();

        public int Salvar(DTO_Usuarios dto)
        {
            if (dto.Nome_User.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório!");
            }
            if (dto.Obeservacao.Trim() == string.Empty)
            {
                throw new ArgumentException("É obrigatório escrever uma observação sobre o usuário!");
            }
            if (dto.Usuario.Trim() == string.Empty)
            {
                throw new ArgumentException("É obrigatório ter um Nickname de usuário!");
            }
            if (dto.Senha.Trim() == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório!");
            }
            Database_Usuario db = new Database_Usuario();
            bool usuario = db.VerificarUsuario(dto);
            if (usuario == true)
            {
                throw new ArgumentException("Este usuário já consta em nossa base de dados!");
            }
            return db.Salvar(dto);
        }
        public void Remover(int id)
        {
            Database_Usuario db = new Database_Usuario();
            db.Remover(id);
        }
        public void Alterar(DTO_Usuarios dto)
        {
            Database_Usuario db = new Database_Usuario();
            bool equivalente = db.VerificarSenha(dto);
            if (equivalente == true)
            {
                throw new ArgumentException("Senha atual encontra-se incorreta!");
                //Mas que sacada!!!
            }
            db.Alterar(dto);
        }
        public DTO_Usuarios Logar(DTO_Usuarios dto)
        {
            if (dto.Usuario.Trim() == string.Empty)
            {
                throw new ArgumentException("Login precisa ser preenchido");
            }
            if (dto.Senha.Trim() == string.Empty)
            {
                throw new ArgumentException("Senha precisa ser preenchido");
            }
            bool errou = validar.Senha(dto.Senha);
            if (errou == true)
            {
                throw new ArgumentException("Senha muito fraca, não esqueça de usar pelo menos:"
                                            + "\n\n" + "Uma letra Maiúscula"
                                            + "\n" + "Uma letra Minúscula"
                                            + "\n" + "Um Número"
                                            + "\n" + "Um Caractere Especial");
            }
            Database_Usuario db = new Database_Usuario();
            return db.Logar(dto);
        }
        public List<DTO_Usuarios> Listar()
        {
            Database_Usuario db = new Database_Usuario();
            List<DTO_Usuarios> list = db.Listar();

            return list;
        }
        public List<DTO_Usuarios> Consultar(DTO_Usuarios dto)
        {
            Database_Usuario db = new Database_Usuario();
            List<DTO_Usuarios> list = db.Consultar(dto);

            return list;
        }

    }
}
