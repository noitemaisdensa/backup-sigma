﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Cadastro_Usuario
{
    public class Business_Departamento
    {
        public List<DTO_Departamento> Listar()
        {
            Database_Departamento db = new Database_Departamento();
            List<DTO_Departamento> list = db.Listar();

            return list;
        }
    }
}
