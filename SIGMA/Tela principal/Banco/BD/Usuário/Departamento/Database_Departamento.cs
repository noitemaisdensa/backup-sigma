﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Cadastro_Usuario
{
    public class Database_Departamento
    {
        public List<DTO_Departamento> Listar()
        {
            string script = @"SELECT * FROM tb_departamento";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Departamento> fora = new List<DTO_Departamento>();

            while(reader.Read())
            {
                DTO_Departamento dentro = new DTO_Departamento();
                dentro.ID = reader.GetInt32("id_departamento");
                dentro.Departamento = reader.GetString("nm_departamento");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
