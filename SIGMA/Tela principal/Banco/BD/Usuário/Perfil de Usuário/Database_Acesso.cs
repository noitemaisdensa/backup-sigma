﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Login
{
    class Database_Acesso
    {
        public int Salvar(DTO_Acesso dto)
        {

            /*
             Qual o sentido de só ter um campo para definir acesso na tela e existir no banco 
             quatro campos para adicionar as permissões de usuario . é necessario modificar o modo
             como foi planejado para ocorrer o funcionamento da permissao de usuario no sistema visto
             que não está funcionando corretamente.            
             */
            string script =
            @"INSERT INTO  tb_acesso
            (
                id_usuario,
                bt_acesso_menu,
                bt_inserir_dados,
                bt_alterar_dados,
                bt_excluir_dados
            )
            VALUES
            (
                @id_usuario,
                @bt_acesso_menu,
                @bt_inserir_dados,
                @bt_alterar_dados,
                @bt_excluir_dados
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_usuario", dto.ID_Usuario));
            parm.Add(new MySqlParameter("bt_acesso_menu", dto.Acesso_Menu));
            parm.Add(new MySqlParameter("bt_inserir_dados", dto.Inserir_Dados));
            parm.Add(new MySqlParameter("bt_alterar_dados", dto.Alterar_Dados));
            parm.Add(new MySqlParameter("bt_excluir_dados", dto.Excluir_Dados));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar(DTO_Acesso dto)
        {
            string script =
                @"UPDATE tb_acesso
                     SET id_usuario         =   @id_usuario,
                         bt_acesso_menu     =   @bt_acesso_menu,
                         bt_inserir_dados   =   @bt_acesso_menu,
                         bt_alterar_dados   =   @bt_alterar_dados,
                         bt_excluir_dados   =   @bt_excluir_dados
                   WHERE id_acesso          =   @id_acesso";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_acesso", dto.ID));
            parm.Add(new MySqlParameter("id_usuario", dto.ID_Usuario));
            parm.Add(new MySqlParameter("bt_acesso_menu", dto.Acesso_Menu));
            parm.Add(new MySqlParameter("bt_inserir_dados", dto.Inserir_Dados));
            parm.Add(new MySqlParameter("bt_alterar_dados", dto.Alterar_Dados));
            parm.Add(new MySqlParameter("bt_excluir_dados", dto.Excluir_Dados));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public void Remover(int dto)
        {
            string script = @"DELETE FROM tb_acesso
                            WHERE id_acesso = @id_acesso";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_acesso", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public List<DTO_Acesso> Listar()
        {
            string script = @"SELECT * FROM tb_acesso";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Acesso> fora = new List<DTO_Acesso>();

            while (reader.Read())
            {
                DTO_Acesso dentro = new DTO_Acesso();
                dentro.ID = reader.GetInt32("id_acesso");
                dentro.ID_Usuario = reader.GetInt32("id_usuario");
                dentro.Acesso_Menu = reader.GetBoolean("bt_acesso_menu");
                dentro.Inserir_Dados = reader.GetBoolean("bt_inserir_dados");
                dentro.Alterar_Dados = reader.GetBoolean("bt_alterar_dados");
                dentro.Excluir_Dados = reader.GetBoolean("bt_excluir_dados");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;

            //Esse método pode ser mudado!
        }
        public DTO_Acesso Logar(DTO_Acesso dto)
        {
            string script = @"SELECT * FROM tb_acesso
                                      WHERE id_usuario = @id_usuario";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_usuario", dto.ID_Usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_Acesso dentro = null;

            if (reader.Read())
            {
                dentro = new DTO_Acesso();
                dentro.ID = reader.GetInt32("id_acesso");
                dentro.ID_Usuario = reader.GetInt32("id_usuario");
                dentro.Acesso_Menu = reader.GetBoolean("bt_acesso_menu");
                dentro.Inserir_Dados = reader.GetBoolean("bt_inserir_dados");
                dentro.Alterar_Dados = reader.GetBoolean("bt_alterar_dados");
                dentro.Excluir_Dados = reader.GetBoolean("bt_excluir_dados");

            }
            reader.Close();
            return dentro;
        }
    }
}
