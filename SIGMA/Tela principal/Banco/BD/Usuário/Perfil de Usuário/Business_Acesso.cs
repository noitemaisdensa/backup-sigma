﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Login
{
    public class Business_Acesso
    {
        public int Salvar (DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            return db.Salvar(dto);
        }
        public void Alterar (DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            db.Alterar(dto);
        }
        public void Remvoer (int dto)
        {
            Database_Acesso db = new Database_Acesso();
            db.Remover(dto);
        }
        public List<DTO_Acesso> Listar ()
        {
            Database_Acesso db = new Database_Acesso();
            List<DTO_Acesso> dto = db.Listar();

            return dto;
        }
        public DTO_Acesso Logar (DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            return db.Logar(dto);
        }
    }
}
