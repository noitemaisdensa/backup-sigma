﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.LModelodoVeiculo
{
    class DTO_Modelo
    {
        public int ID { get; set; }
        public string Modelo { get; set; }
        public string Marca { get; set; }
    }
}
