﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.LModelodoVeiculo
{
    class Business_Modelo
    {
        public List<DTO_Modelo> ConsultarCodigo(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("O código de busca precisa ser preenchido!");
            }

            Database_Modelo db = new Database_Modelo();
            List<DTO_Modelo> list = db.ConsultarCodigo(id);

            return list;
        }

        public List<DTO_Modelo> ConsultarModelo(string modelo)
        {
            if (modelo == string.Empty)
            {
                throw new ArgumentException("O Modelo de busca precisa ser preenchido!");
            }

            Database_Modelo db = new Database_Modelo();
            List<DTO_Modelo> list = db.ConsultarModelo(modelo);

            return list;
        }


        public List<DTO_Modelo> Listar()
        {
            Database_Modelo db = new Database_Modelo();
            List<DTO_Modelo> list = db.Listar();

            return list;
        }
    }
}
