﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Tela_principal.DB;

namespace Tela_principal.LModelodoVeiculo
{
    class Database_Modelo
    {
        public List<DTO_Modelo> ConsultarCodigo (int id)
        {
            string script = 
                @"select * from tb_modelo
                          WHERE id_modelo = @id_modelo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_modelo", id));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Modelo> lista = new List<DTO_Modelo>();

            while(reader.Read())
            {
                DTO_Modelo dentro = new DTO_Modelo();
                dentro.ID = reader.GetInt32("id_modelo");
                dentro.Marca = reader.GetString("ds_marca");
                dentro.Modelo = reader.GetString("ds_modelo");

                lista.Add(dentro);
            }
            reader.Close();
            return lista;
        }
        public List<DTO_Modelo> ConsultarModelo (string modelo)
        {
            string script =
                @"select * from tb_modelo
                          WHERE ds_modelo LIKE @ds_modelo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + modelo + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Modelo> lista = new List<DTO_Modelo>();

            while (reader.Read())
            {
                DTO_Modelo dentro = new DTO_Modelo();
                dentro.ID = reader.GetInt32("id_modelo");
                dentro.Marca = reader.GetString("ds_marca");
                dentro.Modelo = reader.GetString("ds_modelo");

                lista.Add(dentro);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Modelo> Listar ()
        {
            string script =
                @"select * from tb_modelo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
          
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Modelo> lista = new List<DTO_Modelo>();

            while (reader.Read())
            {
                DTO_Modelo dentro = new DTO_Modelo();
                dentro.ID = reader.GetInt32("id_modelo");
                dentro.Marca = reader.GetString("ds_marca");
                dentro.Modelo = reader.GetString("ds_modelo");

                lista.Add(dentro);
            }
            reader.Close();
            return lista;
        }

    }
}
