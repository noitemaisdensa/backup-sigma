﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Ordem_de_Servico.Status
{
   public class DTO_Status
    {
        public int ID { get; set; }
        public int ID_Funcionario { get; set; }
        public string Descricao { get; set; }
        public DateTime Data_Inicio { get; set; }
        public DateTime Data_Fim { get; set; }
        public string Hora { get; set; }
        public string Hora2 { get; set; }
        public string Kilometragem { get; set; }
        public string Periodo { get; set; }
        public string Laudo { get; set; }
        public bool  Status { get; set; }

    }
}
