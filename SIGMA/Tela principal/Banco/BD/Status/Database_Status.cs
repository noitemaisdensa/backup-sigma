﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tela_principal.DB;

namespace Tela_principal.Ordem_de_Servico.Status
{
    public class Database_Status
    {
        public int Salvar(DTO_Status dto)
        { 
            string script =
            @"INSERT INTO tb_status
            (
                id_funcionario,
                ds_descricao,
                dt_inicial,
                dt_final,
                dt_hora,
                dt_hora2,
                ds_kilometragem,
                ds_periodo,
                ds_laudo,
                bt_status
            )
            VALUES
            (
                @id_funcionario,
                @ds_descricao,
                @dt_inicial,
                @dt_final,
                @dt_hora,
                @dt_hora2,
                @ds_kilometragem,
                @ds_periodo,
                @ds_laudo,
                @bt_status
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parm.Add(new MySqlParameter("ds_descricao", dto.Descricao));
            parm.Add(new MySqlParameter("dt_inicial", dto.Data_Inicio));
            parm.Add(new MySqlParameter("dt_final", dto.Data_Fim));
            parm.Add(new MySqlParameter("dt_hora", dto.Hora));
            parm.Add(new MySqlParameter("dt_hora2", dto.Hora2));
            parm.Add(new MySqlParameter("ds_kilometragem", dto.Kilometragem));
            parm.Add(new MySqlParameter("ds_periodo", dto.Periodo));
            parm.Add(new MySqlParameter("ds_laudo", dto.Laudo));
            parm.Add(new MySqlParameter("bt_status", dto.Status));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
