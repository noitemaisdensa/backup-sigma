﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Funções.Apenas_Letra
{
    class Caracter_Digito
    {
        //Verifica se há algum número
        public bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
            {
                return true;
            }
            else
            {
                throw new ArgumentException("O campo não aceita valores númericos");
            }

        }
        //Verifica se há algum tipo de caractere
        public bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
            {
                return true;
            }
            else
            {
                throw new ArgumentException("O campo não aceita valores em caracteres");
            }
                


        }
    }
}
