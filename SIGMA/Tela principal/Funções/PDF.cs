﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tela_principal.Funções
{
    class PDF
    {
        public  void pdf(string placa,string total,string carro,string falha,string func) {
            //acessar a biblioteca , dentro do using para eliminar as referencias de memoria quando terminar
            using (var doc = new PdfSharp.Pdf.PdfDocument())
            {
                //criar pagina
                var page = doc.AddPage();
                //criar um grafico 
                var graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(page);
                //para escrever texto 
                var textFormatter = new PdfSharp.Drawing.Layout.XTextFormatter(graphics);
                //criar fonte - nome da fonte e o tamanho
                var font = new PdfSharp.Drawing.XFont("Arial", 14);

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;

                //como colocar imagem 
               // graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(img , 0,0));
                //Escrevendo
                // Pamametros - texto - fonte - cor da fonte - retangulo onde o texto será inserido (coordenadas)
                textFormatter.DrawString(placa, 
                                         font, 
                                         PdfSharp.Drawing.XBrushes.Black, 
                                         new PdfSharp.Drawing.XRect(10,50,page.Width,page.Height));

                graphics.DrawLine(PdfSharp.Drawing.XPens.DarkBlue, 0, 0, 0, 0);
                textFormatter.DrawString(total,
                                        font,
                                        PdfSharp.Drawing.XBrushes.Black,
                                        new PdfSharp.Drawing.XRect(10, 50, page.Width, page.Height));

                graphics.DrawLine(PdfSharp.Drawing.XPens.DarkBlue, 0, 0, 0, 0);
                textFormatter.DrawString(carro,
                                      font,
                                      PdfSharp.Drawing.XBrushes.Black,
                                      new PdfSharp.Drawing.XRect(10, 50, page.Width, page.Height));

                graphics.DrawLine(PdfSharp.Drawing.XPens.DarkBlue, 0, 0, 0, 0);
                textFormatter.DrawString(falha,
                                    font,
                                    PdfSharp.Drawing.XBrushes.Black,
                                    new PdfSharp.Drawing.XRect(10, 50, page.Width, page.Height));

                graphics.DrawLine(PdfSharp.Drawing.XPens.DarkBlue, 0, 0, 0, 0);
                textFormatter.DrawString(func,
                                    font,
                                    PdfSharp.Drawing.XBrushes.Black,
                                    new PdfSharp.Drawing.XRect(10, 50, page.Width, page.Height));



                doc.Save("Orcamento.pdf");
                System.Diagnostics.Process.Start("Orcamento.pdf");
            }

        }
    }
}
