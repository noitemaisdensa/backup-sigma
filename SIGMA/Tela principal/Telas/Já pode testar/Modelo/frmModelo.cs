﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal
{
    public partial class frmModelo : Form
    {
        public frmModelo()
        {
            InitializeComponent();
        }

        DTO_Modelo dto = new DTO_Modelo();
        Business_Modelo db = new Business_Modelo();

        public void ConsultarModelo ()
        {
            try
            {
                int id = dto.ID;
                string modelo = dto.Modelo;
                if (rdnCodigo.Checked == true)
                {
                    id = Convert.ToInt32(txtvalor.Text);
                    List<DTO_Modelo> codigo = db.ConsultarCodigo(id);

                    dvgmodelo.AutoGenerateColumns = false;
                    dvgmodelo.DataSource = codigo;

                }
                else
                {
                    modelo = txtvalor.Text;
                    List<DTO_Modelo> modelos = db.ConsultarModelo(modelo);

                    dvgmodelo.AutoGenerateColumns = false;
                    dvgmodelo.DataSource = modelos;
                }

                lblRef.Text = dvgmodelo.Rows.Count.ToString();
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!/n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!/n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultarModelo();
        }

        private void txtvalor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ConsultarModelo();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
                this.Hide();
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
