﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Banco.Plugin;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Cadastro_Funcionario.Tela;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Objetos.UF;

namespace Tela_principal.Telas.Funcionários
{
    public partial class frmAlterar_Funcionarios : Form
    {
        public frmAlterar_Funcionarios()
        {
            InitializeComponent();
            CarregarCombo();

            UF uf = new UF();
            cboUF.DataSource = uf.UFS();
        }

        public void CarregarCombo()
        {
            Business_Departamento db = new Business_Departamento();
            List<DTO_Departamento> departamento = db.Listar();

            cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            cboDepartamento.DataSource = departamento;

            /* Depois de pensar MUITO a respeito desse erro, pude concluir que o erro, na verdade, é bem simples!
             * O erro encontra-se pelo fato de a combobox, na hora em que a mesma deve pegar o valor do ID_Departamento
             * Vindo do método LoadScreen, ela acaba por não consegui pegar o valor correto.
             * Isso ocorre, acredito eu, pelo fato da mesma não consegui encontrar uma referência direta no C#
             * Tendo em Vista que os valores da tabela Departamento já está embutida no banco de dados.
             * Simplificando... A combobox não consegue encontrar o valor, pois o mesmo vem direto do banco de dados.
             */
        }
        
        DTO_Funcionario dto;
        public void LoadScreen(DTO_Funcionario dto)
        {
            this.dto = dto;
            
            //Passando os valores para os controles
            cboDepartamento.SelectedItem = dto.ID_Departamento;
            lblNumero.Text = dto.ID.ToString();
            txtNome.Text = dto.Nome;
            txtCPF.Text = dto.CPF;
            dtpNascimento.Value = dto.DatadeNascimento;
            txtEndereco.Text = dto.Endereco;
            txtCEP.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF;
            txtComplemento.Text = dto.Complemento;
            txtCidade.Text = dto.Cidade;    
            nudNumero.Value = dto.Numero;
            txtEmail.Text = dto.Email;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
            txtTelefoneResidencial.Text = dto.TelefoneResidencial;
            imgFoto.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);

            //Mulher?
            if (dto.Sexo == true)
            {
                rdnF.Checked = true;
            }
            //Homem?
            else 
            {
                rdnM.Checked = true;
            }
        }



        public void AlterarFuncionarios ()
        {
            //try
            //{
                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                dto.ID_Departamento = departamento.ID;
                dto.Nome = txtNome.Text;
                dto.CPF = txtCPF.Text;
                dto.DatadeNascimento = dtpNascimento.Value;
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = nudNumero.Value;
                dto.Cidade = txtCidade.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.TelefoneResidencial = txtTelefoneResidencial.Text;
                dto.Celular = txtCelular.Text;
                dto.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);

                if (rdnF.Checked == true)
                {
                    dto.Sexo = true;
                }
                else
                {
                    dto.Sexo = false;
                }

                Business_Funcionario db = new Business_Funcionario();
                db.Alterar(dto);
                MessageBox.Show("As informações do Funcionário foram alteradas com sucesso",
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Information);
            //}
            //catch (ArgumentException ex)
            //{
            //    MessageBox.Show("ERRO!" + "\n" + ex.Message,
            //                     "Sigma",
            //                     MessageBoxButtons.OK,
            //                     MessageBoxIcon.Error);
            //}
            //catch (Exception erro)
            //{
            //    MessageBox.Show("ERRO!" + "\n" + erro.Message,
            //                     "Sigma",
            //                     MessageBoxButtons.OK,
            //                     MessageBoxIcon.Error);
            //}
        }

        private void btnCaminho_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFoto.ImageLocation = dialog.FileName;
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            AlterarFuncionarios();
        }

        private void txtNome_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AlterarFuncionarios();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Funcionario tela = new frmConsultar_Funcionario();
            tela.Show();
        }

        private void lblFechar_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }
    }
}
