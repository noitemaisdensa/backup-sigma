﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Orçamento;
using Tela_principal.Telas.Funcionários;

namespace Tela_principal.Cadastro_Funcionario.Tela
{
    public partial class frmConsultar_Funcionario : Form
    {
        
        

        public frmConsultar_Funcionario()
        {
            InitializeComponent();
            CarregarComboGrid();

        }

        public void CarregarComboGrid()
        {
            //Com a business instaciada, faço a chamada do método de Listar
            Business_Departamento business = new Business_Departamento();
            List<DTO_Departamento> list = business.Listar();

            //Pego o ID do Combobox da GriedView
            ColumnDepartamento.ValueMember = nameof(DTO_Departamento.ID);

            //Passo o valor do DTO para a Combobox da GriedView
            ColumnDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            //Passo o valor do Método Listar
            ColumnDepartamento.DataSource = list;
        }

        public void CarregarGrid()
        {
            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Nome = txtPesquisa.Text;

            Business_Funcionario db = new Business_Funcionario();
            List<DTO_Funcionario> consult = db.Consultar(dto);

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = consult;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                DTO_Funcionario linha = dgvFuncionario.CurrentRow.DataBoundItem as DTO_Funcionario;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja alterar as infomações desse funcionário?",
                                                      "Aviso",
                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    this.Hide();
                    frmAlterar_Funcionarios tela = new frmAlterar_Funcionarios();
                    tela.LoadScreen(linha);
                    tela.Show();

                }
            }
            if (e.ColumnIndex == 4)
            {
                DTO_Funcionario linha = dgvFuncionario.CurrentRow.DataBoundItem as DTO_Funcionario;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja excluir esse funcionario?",
                                                       "Aviso",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    Business_Funcionario db = new Business_Funcionario();
                    db.Remover(linha.ID);

                    CarregarGrid();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrar_Funcionario tela = new frmCadastrar_Funcionario();
            tela.Show();
        }
    }
}


