﻿using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Banco.Plugin;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Cadastro_Funcionario.Tela;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Funções.Apenas_Letra;
using Tela_principal.Objetos.UF;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Orçamento
{
    public partial class frmCadastrar_Funcionario : Form
    {


        public frmCadastrar_Funcionario()
        {
            InitializeComponent();
            CarregarCombos();

            UF uf = new UF();
            cboUF.DataSource = uf.UFS();
        }

        public void CarregarCombos()
        {
            Business_Departamento db = new Business_Departamento();
            List<DTO_Departamento> departamento = db.Listar();

            cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            cboDepartamento.DataSource = departamento;
        }

        public void SalvarFuncionario()
        {
            try
            {
                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                DTO_Funcionario dto = new DTO_Funcionario();
                dto.ID_Departamento = departamento.ID;
                dto.Nome = txtNome.Text;
                dto.CPF = txtCPF.Text;
                dto.DatadeNascimento = dtpNascimento.Value;
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = nudNumero.Value;
                dto.Cidade = txtCidade.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.TelefoneResidencial = txtTelefoneResidencial.Text;
                dto.Celular = txtCelular.Text;
                dto.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);

                if (rdnF.Checked == true)
                {
                    dto.Sexo = true;
                }
                else
                {
                    dto.Sexo = false;
                }

                Business_Funcionario db = new Business_Funcionario();
                db.Salvar(dto);
                MessageBox.Show("Funcionário cadastrado com sucesso",
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public void TelaInicial()
        {
            try
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Hide();

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }

        private void btnCaminho_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png|AllFiles(*.*)|*.*";
                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    imgFoto.ImageLocation = dialog.FileName;
                }
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void label23_Click(object sender, EventArgs e)
        {
            TelaInicial();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            TelaInicial();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            SalvarFuncionario();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Hide();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmConsultar_Funcionario tela = new frmConsultar_Funcionario();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CorreiosApi service = new CorreiosApi();
                var cep = txtCEP.Text;
                var dados = service.consultaCEP(cep);

                string rua = dados.end;
                string bairro = dados.bairro;
                string cidade = dados.cidade;

                string endereco = rua + ", " + bairro;
                txtCidade.Text = cidade;
                txtEndereco.Text = endereco;

                cboUF.SelectedItem = dados.uf;
            }
            catch (Exception)
            {

                MessageBox.Show("CEP inválido, digite novamente!",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void btnSalvar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SalvarFuncionario();
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SalvarFuncionario();
            }
        }
    }
}
