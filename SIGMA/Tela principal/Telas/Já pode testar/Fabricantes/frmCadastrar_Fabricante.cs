﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.CadastrarFabricantes;

namespace Tela_principal.Sub_Telas
{
    public partial class frmCadastrar_Fabricantes : Form
    {
        public frmCadastrar_Fabricantes()
        {
            InitializeComponent();
        }

        public void Salvar ()
        {
            try
            {

                DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
                dto.Nome = txtNome.Text;
                dto.CNPJ = txtCPNPJ.Text;
                dto.Descricao = rtxtDescricao.Text;

                Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();
                db.Salvar(dto);

                MessageBox.Show("Fabricante cadastrado com sucesso!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtCPNPJ.Text == "  .   .   /    -")
                {
                    MessageBox.Show("CNPJ não pode estar vázio",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    return;
                }

               DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
               dto.Nome = txtNome.Text.Trim();
               dto.CNPJ = txtCPNPJ.Text;
               dto.Descricao = rtxtDescricao.Text.Trim();

               Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();
               db.Salvar(dto);

               MessageBox.Show("Fabricante cadastrado com sucesso!",
                               "Sigma",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Information);
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }

        private void rtxtDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Salvar();
            }
        }

        private void txtCPNPJ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Salvar();
            }

        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Salvar();
            }

        }
    }
}
/*
    Bom, o que eu pude notar de estranho nessa parte do programam é que necessitamos validar esse CNPJ...
    Outro sim, precisamos também limitar a duplicidade de fornecedores com as mesmas credênciais.

    OBS: Peço para que todos que irão programar nesse projeto, se possível, encontrar possíveis erros!
 */ 
  