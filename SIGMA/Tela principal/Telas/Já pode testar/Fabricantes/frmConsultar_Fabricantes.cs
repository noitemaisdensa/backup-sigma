﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Sub_Telas;
using Tela_principal.Telas.Fabricantes;

namespace Tela_principal.CadastrarFabricantes
{
    public partial class frmFabricante_consultar_excluir_alterar : Form
    {

        Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();

        public frmFabricante_consultar_excluir_alterar()
        {
            InitializeComponent();

        }
        public void CarregarGrid()
        {
            try
            {
                DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
                dto.Nome = txtPesquisa.Text;

                
                List<DTO_Cadastrar_Fabricantes> list = db.Consultar(dto);

                dgvFabricante.AutoGenerateColumns = false;
                dgvFabricante.DataSource = list;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar os fabricantes: " + ex.Message, "Sigma",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvFabricante_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                //Valor da Gried
                DTO_Cadastrar_Fabricantes linha = dgvFabricante.CurrentRow.DataBoundItem as DTO_Cadastrar_Fabricantes;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja alterar as infomações desse fabricante?",
                                                      "ATENÇÃO!!",
                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    this.Hide();
                    frmAlterar_Fabricantes tela = new frmAlterar_Fabricantes();

                    //Chamada do método LoadScreen
                    tela.LoadScreen(linha);

                    tela.Show();
                }
            }

            if (e.ColumnIndex == 4)
            {
                //Valor da Gried
                DTO_Cadastrar_Fabricantes linha = dgvFabricante.CurrentRow.DataBoundItem as DTO_Cadastrar_Fabricantes;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja excluir esse fabricante?",
                                                       "ATENÇÃO!!",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    db.Remover(linha.ID);

                    CarregarGrid();
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrar_Fabricantes tela = new frmCadastrar_Fabricantes();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void txtPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }
    }
}
