﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.CadastrarFabricantes;
using Tela_principal.Objetos.Validadores;
using Tela_principal.Orçamento;

namespace Tela_principal.Sub_Telas
{
    public partial class frmCadastrar_Fabricantes : Form
    {
        public frmCadastrar_Fabricantes()
        {
            InitializeComponent();
        }

        Validacao validar = new Validacao();

        public void Salvar()
        {
            try
            {
                DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
                dto.Nome = txtNome.Text;
                dto.Descricao = rtxtDescricao.Text;
                dto.CNPJ = txtCPNPJ.Text;

                if (validar.VerificarCNPJ(dto.CNPJ) == false)
                {
                    MessageBox.Show("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }

                Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();
                db.Salvar(dto);

                MessageBox.Show("Fabricante cadastrado com sucesso!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Salvar();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                this.Hide();
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmFabricante_consultar_excluir_alterar tela = new frmFabricante_consultar_excluir_alterar();
            tela.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmOrcamento tela = new frmOrcamento();
            tela.Show();
        }

        private void txtNome_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Salvar();
            }
        }
    }
}

