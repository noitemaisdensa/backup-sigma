﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.CadastrarFabricantes
{
    public partial class frmFabricante_consultar_excluir_alterar : Form
    {
        Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();

        public frmFabricante_consultar_excluir_alterar()
        {
            InitializeComponent();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
            dto.Nome = txtPesquisa.Text;

            List<DTO_Cadastrar_Fabricantes> list = db.Consultar(dto);

            dgvFabricante.AutoGenerateColumns = false;
            dgvFabricante.DataSource = list;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
            dto.Nome = txtNome.Text;
            dto.CNPJ = txtCPNPJ.Text;
            dto.Descricao = rtxtDescricao.Text;

            db.Alterar(dto);
        }

        private void dgvFabricante_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja excluir esse fabricante?",
                                                       "ATENÇÃO!!",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                    DTO_Cadastrar_Fabricantes linha = dgvFabricante.CurrentRow.DataBoundItem as DTO_Cadastrar_Fabricantes;
                    int id = linha.ID;

                    db.Remover(id);
                }

            }

            if (e.ColumnIndex == 4)
            {

                DialogResult dialog = MessageBox.Show("Tem certeza que deseja alterar as infomações desse fabricante?",
                                                      "ATENÇÃO!!",
                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                    txtNome.Enabled = true;
                    txtCPNPJ.Enabled = true;
                    txtPesquisa.Enabled = true;
                    DTO_Cadastrar_Fabricantes dto = new DTO_Cadastrar_Fabricantes();
                    dto.Nome = txtNome.Text;
                    dto.CNPJ = txtCPNPJ.Text;
                    dto.Descricao = rtxtDescricao.Text;

                    db.Alterar(dto);
                }

            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            this.Close();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {

        }

        /*
            Bom... Nessa parte do programa há muito a se fazer e a entender...
            Infelizmente, chega um momento do dia que eu fico muito bocó, e eu simplesmente não consigo programar
            mas devemos arrumar essa parte por inteiro.

            Começando pelo erro de não encontro de referência entre a GRID e o DTO.
            Segundo, COmo irá funcionar a parte de alterar? (Talvez seja o sono, mas eu não entendi como iria funcionar)
            Terceiro, Se a parte de alterar for aquela bem abaixo, devemos colocar um validador de CNPJ
            Quarto, devemos habilitar todos os controles que permitem ao usuário fazer a navegação entre as telas...
            É isso que eu pude notar nessa tela.

            Obs: Uma vez mais eu pondero, se quem estiver programando encontrar algum bug, por favor, comunique-se com
            a senhorita Alessandra, para assim, tentarmos abordar e tentar corrigir os possíveis erros!

        */
    }
}
