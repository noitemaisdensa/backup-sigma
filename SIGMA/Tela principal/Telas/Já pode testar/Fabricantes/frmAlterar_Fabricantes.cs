﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.CadastrarFabricantes;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Telas.Fabricantes
{
    public partial class frmAlterar_Fabricantes : Form
    {
        public frmAlterar_Fabricantes()
        {
            InitializeComponent();
        }

        Validacao validar = new Validacao();
        DTO_Cadastrar_Fabricantes dto;

        public void LoadScreen(DTO_Cadastrar_Fabricantes dto)
        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            txtCPNPJ.Text = dto.CNPJ;
            rtxtDescricao.Text = dto.Descricao;
        }
        public void AlterarFabricantes()
        {
            try
            {
                dto.Nome = txtNome.Text;
                dto.Descricao = rtxtDescricao.Text;
                dto.CNPJ = txtCPNPJ.Text;

                Business_Cadastro_Fabricante db = new Business_Cadastro_Fabricante();
                db.Alterar(dto);

                MessageBox.Show("As informações do fabricante foram alterados com sucesso!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            AlterarFabricantes();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmFabricante_consultar_excluir_alterar tela = new frmFabricante_consultar_excluir_alterar();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AlterarFabricantes();
            }
        }
    }
}
