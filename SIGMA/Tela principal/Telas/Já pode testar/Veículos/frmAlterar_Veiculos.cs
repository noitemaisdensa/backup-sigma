﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Cadastro_de_veiculos;
using Tela_principal.Cadastro_Veiculos;
using Tela_principal.Funções.Anos;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal.Telas.Veículos
{
    public partial class frmAlterar_Veiculos : Form
    {
        public frmAlterar_Veiculos()
        {
            InitializeComponent();
            //Ano
            Ano ano = new Ano();
            cboAnoFabricacao.DataSource = ano.Anos();

            //Combustível
            List<string> combo = new List<string>();
            combo.Add("Diesel");
            combo.Add("Etánol");
            combo.Add("Gasolina");
            combo.Add("Gás Natural");
            cboCombustivel.DataSource = combo;

            //Combos
            CarregarCombos();
        }

        DTO_Veiculos dto;

        public void LoadScreen (DTO_Veiculos dto)
        {
            this.dto = dto;

            cboProprietario.SelectedItem = dto.ID_Cliente;
            txtPlaca.Text = dto.Placa;
            cboModelo.SelectedItem = dto.ID_Modelo;
            cboCombustivel.SelectedItem = dto.Combustivel;
            nudOdometro.Value = dto.Odometro;
            txtCor.Text = dto.Cor;
            cboAnoFabricacao.SelectedItem = dto.Ano;
            rtbObservacao.Text = dto.Observacoes;
        }

        public void CarregarCombos()
        {
            try
            {  
                Business_Modelo db = new Business_Modelo();
                List<DTO_Modelo> list = db.Listar();

                cboModelo.ValueMember = nameof(DTO_Modelo.ID);
                cboModelo.DisplayMember = nameof(DTO_Modelo.Modelo);

                cboModelo.DataSource = list;

                // ---------------OUTRA COMBO-------------//

                Business_Cliente dados = new Business_Cliente();
                List<DTO_Cliente> lista = dados.Listar();

                cboProprietario.ValueMember = nameof(DTO_Cliente.ID);
                cboProprietario.DisplayMember = nameof(DTO_Cliente.Nome);
                           
                cboProprietario.DataSource = lista;
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }
        public void AlterarVeiculo ()
        {
            try
            {
                DTO_Modelo modelo = cboModelo.SelectedItem as DTO_Modelo;
                DTO_Cliente cliente = cboProprietario.SelectedItem as DTO_Cliente;

                dto.Placa = txtPlaca.Text; //Relacionamentos
                dto.ID_Modelo = modelo.ID; //Relacionamentos
                dto.ID_Cliente = cliente.ID; //Relacionamentos
                dto.Combustivel = cboCombustivel.Text.ToString();
                dto.Odometro = Convert.ToInt32(nudOdometro.Value);
                dto.Cor = txtCor.Text;
                dto.Ano = cboAnoFabricacao.SelectedItem.ToString();
                dto.Observacoes = rtbObservacao.Text;

                Business_Veiculos db = new Business_Veiculos();
                db.Alterar(dto);

                MessageBox.Show("As informações do veículo foram Alteradas com sucesso",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        private void txtPlaca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AlterarVeiculo();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlterarVeiculo();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Veiculos tela = new frmConsultar_Veiculos();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }
    }
}
