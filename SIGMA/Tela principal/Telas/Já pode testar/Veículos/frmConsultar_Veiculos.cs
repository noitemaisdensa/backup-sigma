﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Cadastro_de_veiculos;
using Tela_principal.LModelodoVeiculo;
using Tela_principal.Telas.Veículos;

namespace Tela_principal.Cadastro_Veiculos
{
    public partial class frmConsultar_Veiculos : Form
    {
        public frmConsultar_Veiculos()
        {
            InitializeComponent();
            CarregarComboGrid();
        }
        public void CarregarComboGrid()
        {
            Business_Modelo db = new Business_Modelo();
            List<DTO_Modelo> modelo = db.Listar();

            columnModelo.ValueMember = nameof(DTO_Modelo.ID);
            columnModelo.DisplayMember = nameof(DTO_Modelo.Modelo);

            columnModelo.DataSource = modelo;
        }
        public void CarregarGrid ()
        {
            Business_Veiculos db = new Business_Veiculos();
            List<DTO_Veiculos> listar = db.Listar();

            dgvVeiculo.AutoGenerateColumns = false;
            dgvVeiculo.DataSource = listar;


        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DTO_Veiculos linha = dgvVeiculo.CurrentRow.DataBoundItem as DTO_Veiculos;
                DialogResult resp = MessageBox.Show("Deseja alterar as informações do Veiculo ?",
                                                    "Aviso",
                                                    MessageBoxButtons.YesNo,MessageBoxIcon.Question);

                if (resp == DialogResult.Yes)
                {
                    this.Hide();
                    frmAlterar_Veiculos tela = new frmAlterar_Veiculos();
                    tela.LoadScreen(linha);
                    tela.Show();
                }
                
            }

            if (e.ColumnIndex == 5)
            {
                DTO_Veiculos linha = dgvVeiculo.CurrentRow.DataBoundItem as DTO_Veiculos;
                DialogResult resp = MessageBox.Show("Tem certeza que deseja excluir este veiculo?" + "\n" +
                                                       "Isso não excluirá o cliente",
                                                       "Aviso",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (resp == DialogResult.Yes)
                {
                    Business_Veiculos db = new Business_Veiculos();
                    db.Remover(linha.ID);

                    CarregarGrid();

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastro_Veiculos tela = new frmCadastro_Veiculos();
            tela.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void button2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }
    }
}
