﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Cadastro_Veiculos;
using Tela_principal.Funções.Anos;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal.Cadastro_de_veiculos
{
    public partial class frmCadastro_Veiculos : Form
    {
        DTO_Cliente cliente = new DTO_Cliente();

        public frmCadastro_Veiculos()
        {
            InitializeComponent();
            CarregarCombos();

            List<string> combo = new List<string>();
            combo.Add("Diesel");
            combo.Add("Etánol");
            combo.Add("Gasolina");
            combo.Add("Gás Natural");

            cboCombustivel.DataSource = combo;

            Ano ano = new Ano();
            cboAnoFabricacao.DataSource = ano.Anos();

        }
        public void CarregarCombos()
        {
            try
            {
                Business_Modelo db = new Business_Modelo();
                List<DTO_Modelo> list = db.Listar();

                cboModelo.ValueMember = nameof(DTO_Modelo.ID);
                cboModelo.DisplayMember = nameof(DTO_Modelo.Modelo);

                cboModelo.DataSource = list;

                // ---------------OUTRA COMBO-------------//

                Business_Cliente dados = new Business_Cliente();
                List<DTO_Cliente> lista = dados.Listar();

                cboProprietario.ValueMember = nameof(DTO_Cliente.ID);
                cboProprietario.DisplayMember = nameof(DTO_Cliente.Nome);

                cboProprietario.DataSource = lista;
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void cboProprietário_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cliente = cboProprietario.SelectedItem as DTO_Cliente;

            lblNumeroCodigo.Text = cliente.ID.ToString();
        }

        public void Automagicamente()
        {
            try
            {
                DTO_Veiculos dto = new DTO_Veiculos();
                DTO_Modelo modelo = cboModelo.SelectedItem as DTO_Modelo;
                this.cliente = cboProprietario.SelectedItem as DTO_Cliente;

                dto.Placa = txtPlaca.Text; //Relacionamentos
                dto.ID_Modelo = modelo.ID; //Relacionamentos
                dto.ID_Cliente = cliente.ID; //Relacionamentos
                dto.Combustivel = cboCombustivel.Text.ToString();
                dto.Odometro = Convert.ToInt32(nudOdometro.Value);
                dto.Cor = txtCor.Text;
                dto.Ano = cboAnoFabricacao.SelectedItem.ToString();
                dto.Observacoes = rtbObservacao.Text;

                Business_Veiculos db = new Business_Veiculos();
                db.Salvar(dto);

                MessageBox.Show("Veículo Salvo com sucesso",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public void TelaInicial()
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            TelaInicial();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            TelaInicial();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }

        private void txtPlaca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Automagicamente();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            TelaInicial();

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Veiculos tela = new frmConsultar_Veiculos();
            tela.Show();
        }
    }
}
