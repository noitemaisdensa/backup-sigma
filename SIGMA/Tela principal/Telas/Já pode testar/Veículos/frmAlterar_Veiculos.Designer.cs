﻿namespace Tela_principal.Telas.Veículos
{
    partial class frmAlterar_Veiculos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlterar_Veiculos));
            this.lblObservacao = new System.Windows.Forms.Label();
            this.cboProprietario = new System.Windows.Forms.ComboBox();
            this.txtPlaca = new System.Windows.Forms.MaskedTextBox();
            this.cboModelo = new System.Windows.Forms.ComboBox();
            this.cboAnoFabricacao = new System.Windows.Forms.ComboBox();
            this.lblAnoFabricacao = new System.Windows.Forms.Label();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.lblCor = new System.Windows.Forms.Label();
            this.lblOdometro = new System.Windows.Forms.Label();
            this.cboCombustivel = new System.Windows.Forms.ComboBox();
            this.lblCombustivel = new System.Windows.Forms.Label();
            this.rtbObservacao = new System.Windows.Forms.RichTextBox();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblProprietario = new System.Windows.Forms.Label();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.lblFechar = new System.Windows.Forms.Label();
            this.nudOdometro = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudOdometro)).BeginInit();
            this.SuspendLayout();
            // 
            // lblObservacao
            // 
            this.lblObservacao.AutoSize = true;
            this.lblObservacao.Location = new System.Drawing.Point(210, 27);
            this.lblObservacao.Name = "lblObservacao";
            this.lblObservacao.Size = new System.Drawing.Size(73, 13);
            this.lblObservacao.TabIndex = 72;
            this.lblObservacao.Text = "Observações:";
            // 
            // cboProprietario
            // 
            this.cboProprietario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboProprietario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProprietario.FormattingEnabled = true;
            this.cboProprietario.Location = new System.Drawing.Point(78, 24);
            this.cboProprietario.Name = "cboProprietario";
            this.cboProprietario.Size = new System.Drawing.Size(121, 21);
            this.cboProprietario.TabIndex = 57;
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(78, 51);
            this.txtPlaca.Mask = "aaa-0000";
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(59, 20);
            this.txtPlaca.TabIndex = 58;
            this.txtPlaca.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlaca_KeyDown);
            // 
            // cboModelo
            // 
            this.cboModelo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModelo.FormattingEnabled = true;
            this.cboModelo.Location = new System.Drawing.Point(76, 80);
            this.cboModelo.Name = "cboModelo";
            this.cboModelo.Size = new System.Drawing.Size(121, 21);
            this.cboModelo.TabIndex = 59;
            // 
            // cboAnoFabricacao
            // 
            this.cboAnoFabricacao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboAnoFabricacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAnoFabricacao.FormattingEnabled = true;
            this.cboAnoFabricacao.Location = new System.Drawing.Point(76, 191);
            this.cboAnoFabricacao.Name = "cboAnoFabricacao";
            this.cboAnoFabricacao.Size = new System.Drawing.Size(82, 21);
            this.cboAnoFabricacao.TabIndex = 63;
            // 
            // lblAnoFabricacao
            // 
            this.lblAnoFabricacao.AutoSize = true;
            this.lblAnoFabricacao.Location = new System.Drawing.Point(7, 189);
            this.lblAnoFabricacao.Name = "lblAnoFabricacao";
            this.lblAnoFabricacao.Size = new System.Drawing.Size(63, 26);
            this.lblAnoFabricacao.TabIndex = 71;
            this.lblAnoFabricacao.Text = "Ano de \r\nFabricação:\r\n";
            // 
            // txtCor
            // 
            this.txtCor.Location = new System.Drawing.Point(76, 165);
            this.txtCor.MaxLength = 20;
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(101, 20);
            this.txtCor.TabIndex = 62;
            this.txtCor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlaca_KeyDown);
            // 
            // lblCor
            // 
            this.lblCor.AutoSize = true;
            this.lblCor.Location = new System.Drawing.Point(44, 161);
            this.lblCor.Name = "lblCor";
            this.lblCor.Size = new System.Drawing.Size(26, 13);
            this.lblCor.TabIndex = 70;
            this.lblCor.Text = "Cor:";
            // 
            // lblOdometro
            // 
            this.lblOdometro.AutoSize = true;
            this.lblOdometro.Location = new System.Drawing.Point(14, 139);
            this.lblOdometro.Name = "lblOdometro";
            this.lblOdometro.Size = new System.Drawing.Size(56, 13);
            this.lblOdometro.TabIndex = 69;
            this.lblOdometro.Text = "Odômetro:";
            // 
            // cboCombustivel
            // 
            this.cboCombustivel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboCombustivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCombustivel.FormattingEnabled = true;
            this.cboCombustivel.Location = new System.Drawing.Point(76, 108);
            this.cboCombustivel.Name = "cboCombustivel";
            this.cboCombustivel.Size = new System.Drawing.Size(121, 21);
            this.cboCombustivel.TabIndex = 60;
            // 
            // lblCombustivel
            // 
            this.lblCombustivel.AutoSize = true;
            this.lblCombustivel.Location = new System.Drawing.Point(5, 116);
            this.lblCombustivel.Name = "lblCombustivel";
            this.lblCombustivel.Size = new System.Drawing.Size(67, 13);
            this.lblCombustivel.TabIndex = 68;
            this.lblCombustivel.Text = "Combustivel:";
            // 
            // rtbObservacao
            // 
            this.rtbObservacao.Location = new System.Drawing.Point(213, 47);
            this.rtbObservacao.MaxLength = 400;
            this.rtbObservacao.Name = "rtbObservacao";
            this.rtbObservacao.Size = new System.Drawing.Size(216, 165);
            this.rtbObservacao.TabIndex = 64;
            this.rtbObservacao.Text = "";
            this.rtbObservacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlaca_KeyDown);
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Location = new System.Drawing.Point(27, 84);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(45, 13);
            this.lblModelo.TabIndex = 67;
            this.lblModelo.Text = "Modelo:";
            // 
            // lblProprietario
            // 
            this.lblProprietario.AutoSize = true;
            this.lblProprietario.Location = new System.Drawing.Point(9, 29);
            this.lblProprietario.Name = "lblProprietario";
            this.lblProprietario.Size = new System.Drawing.Size(63, 13);
            this.lblProprietario.TabIndex = 65;
            this.lblProprietario.Text = "Proprietário:";
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Location = new System.Drawing.Point(30, 50);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(37, 13);
            this.lblPlaca.TabIndex = 66;
            this.lblPlaca.Text = "Placa:";
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(280, 227);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 26);
            this.button1.TabIndex = 74;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button7.BackgroundImage")));
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(363, 227);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(66, 26);
            this.button7.TabIndex = 73;
            this.button7.Text = "Cancelar";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(412, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(17, 17);
            this.lblFechar.TabIndex = 75;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // nudOdometro
            // 
            this.nudOdometro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudOdometro.Location = new System.Drawing.Point(76, 137);
            this.nudOdometro.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudOdometro.Name = "nudOdometro";
            this.nudOdometro.Size = new System.Drawing.Size(120, 20);
            this.nudOdometro.TabIndex = 76;
            this.nudOdometro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudOdometro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlaca_KeyDown);
            // 
            // frmAlterar_Veiculos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 265);
            this.Controls.Add(this.nudOdometro);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.lblObservacao);
            this.Controls.Add(this.cboProprietario);
            this.Controls.Add(this.txtPlaca);
            this.Controls.Add(this.cboModelo);
            this.Controls.Add(this.cboAnoFabricacao);
            this.Controls.Add(this.lblAnoFabricacao);
            this.Controls.Add(this.txtCor);
            this.Controls.Add(this.lblCor);
            this.Controls.Add(this.lblOdometro);
            this.Controls.Add(this.cboCombustivel);
            this.Controls.Add(this.lblCombustivel);
            this.Controls.Add(this.rtbObservacao);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblProprietario);
            this.Controls.Add(this.lblPlaca);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAlterar_Veiculos";
            this.Text = "frmAlterar_Veiculos";
            ((System.ComponentModel.ISupportInitialize)(this.nudOdometro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblObservacao;
        private System.Windows.Forms.ComboBox cboProprietario;
        private System.Windows.Forms.MaskedTextBox txtPlaca;
        private System.Windows.Forms.ComboBox cboModelo;
        private System.Windows.Forms.ComboBox cboAnoFabricacao;
        private System.Windows.Forms.Label lblAnoFabricacao;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.Label lblCor;
        private System.Windows.Forms.Label lblOdometro;
        private System.Windows.Forms.ComboBox cboCombustivel;
        private System.Windows.Forms.Label lblCombustivel;
        private System.Windows.Forms.RichTextBox rtbObservacao;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblProprietario;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.NumericUpDown nudOdometro;
    }
}