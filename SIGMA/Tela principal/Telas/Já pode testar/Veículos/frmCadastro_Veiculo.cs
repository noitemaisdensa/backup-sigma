﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.LModelodoVeiculo;

namespace Tela_principal.Cadastro_de_veiculos
{
    public partial class frmCadastro_Veiculos : Form
    {
        DTO_Cliente cliente = new DTO_Cliente();   
        DTO_Veiculos dto = new DTO_Veiculos();
        public frmCadastro_Veiculos()
        {
            InitializeComponent();
            CarregarCombos();
            lblCodigo.Text = cliente.ID.ToString();
      

            List<string> combo = new List<string>();
            combo.Add("Diesel");
            combo.Add("Etánol");
            combo.Add("Gasolina");
            combo.Add("Gás Natural");

            cboCombustivel.DataSource = combo;

            List<string> anos = new List<string>();
            anos.Add("1990");
            anos.Add("1991");
            anos.Add("1992");
            anos.Add("1993");
            anos.Add("1994");
            anos.Add("1995");
            anos.Add("1996");
            anos.Add("1997");
            anos.Add("1998");
            anos.Add("1999");
            anos.Add("2000");
            anos.Add("2001");
            anos.Add("2002");
            anos.Add("2003");
            anos.Add("2004");
            anos.Add("2005");
            anos.Add("2006");
            anos.Add("2007");
            anos.Add("2008");
            anos.Add("2009");
            anos.Add("2010");
            anos.Add("2011");
            anos.Add("2012");
            anos.Add("2013");
            anos.Add("2014");
            anos.Add("2015");
            anos.Add("2016");
            anos.Add("2017");
            anos.Add("2018");

            cboAnoFabricacao.DataSource = anos;
        }

        public void CarregarCombos()
        {
            try
            {

            
            Business_Modelo db = new Business_Modelo();
            List<DTO_Modelo> list = db.Listar();

            cboModelo.ValueMember = nameof(DTO_Modelo.ID);
            cboModelo.DisplayMember = nameof(DTO_Modelo.Modelo);

            cboModelo.DataSource = list;

            // ---------------OUTRA COMBO-------------//

            Business_Cliente dados = new Business_Cliente();
            List<DTO_Cliente> lista = dados.Listar();

            cboProprietário.ValueMember = nameof(DTO_Cliente.ID);
            cboProprietário.DisplayMember = nameof(DTO_Cliente.Nome);

            cboProprietário.DataSource = lista;
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Automagicamente ()
        {
            try
            {

            DTO_Modelo modelo = cboModelo.SelectedItem as DTO_Modelo;
            cliente = cboProprietário.SelectedItem as DTO_Cliente;
               
            dto.Placa = txtPlaca.Text;
            dto.ID_Modelo = modelo.ID; // Relacionamento com tb_modelo
            dto.ID_Cliente = cliente.ID; 
            dto.Combustivel = cboCombustivel.Text.ToString();
            dto.Odometro = Convert.ToInt32(txtOdometro.Text);
            dto.Cor = txtCor.Text;
            dto.Ano = cboAnoFabricacao.SelectedItem.ToString();
            dto.Observacoes = rtbObservacao.Text;

            Business_Veiculos db = new Business_Veiculos();
            db.Salvar(dto);

            MessageBox.Show("Veículo Salvo com sucesso",
                               "Sigma",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!\n " + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Abracadabra()
        {
            
            {
                try
                {

                    DTO_Modelo modelo = cboModelo.SelectedItem as DTO_Modelo;
                    cliente = cboProprietário.SelectedItem as DTO_Cliente;

                    dto.Placa = txtPlaca.Text;
                    dto.ID_Modelo = modelo.ID; // Relacionamento com tb_modelo
                    dto.ID_Cliente = cliente.ID;
                    dto.Combustivel = cboCombustivel.Text.ToString();
                    dto.Odometro = Convert.ToInt32(txtOdometro.Text);
                    dto.Cor = txtCor.Text;
                    dto.Ano = cboAnoFabricacao.SelectedItem.ToString();
                    dto.Observacoes = rtbObservacao.Text;

                    Business_Veiculos db = new Business_Veiculos();
                    db.Alterar(dto);

                    MessageBox.Show("As informações do veículo foram alteradas com sucesso",
                                       "Sigma",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("ERRO!\n " + ex.Message,
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                catch (Exception erro)
                {
                    MessageBox.Show("ERRO!\n " + erro.Message,
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
        }
        public void Desaparecer()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                                            "Projeto Sigma",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    Application.Exit();
                }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                             "Sigma",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error);
            }
        }
        public void Alacazum()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Você realmente deseja deletar esse registro?",
                                "Sigma",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    DTO_Veiculos dto = new DTO_Veiculos();
                    int id = dto.ID;

                    Business_Veiculos db = new Business_Veiculos();
                    db.Remover(id);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        public void Magicamente()
        {
            try
            {
                DialogResult dialog = MessageBox.Show("Você tem certeza que deseja cancelar o cadastro?",
                                                  "Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    this.Hide();

                    frmTela_Principal tela = new frmTela_Principal();
                    tela.Show();
                }

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!\n " + erro.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void txtOdometro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Esse campo aceita apenas Números", 
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }
        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                   "Projeto Sigma",
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Alacazum();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
            this.Close();
        }

    }
}
