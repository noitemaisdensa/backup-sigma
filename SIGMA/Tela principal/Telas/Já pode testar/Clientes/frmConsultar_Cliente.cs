﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Telas;
using Tela_principal.Telas.Clientes;

namespace Tela_principal.Cadastro_Clientes.Tela
{
    public partial class frmConsultar_Cliente : Form
    {
        public frmConsultar_Cliente()
        {
            InitializeComponent();
        }

        public void CarregarGrid()
        {
            try
            {
                //Instaciamento do DTO
                DTO_Cliente dto = new DTO_Cliente();

                //Pegando o valor da textBox
                dto.Nome = textBox1.Text;

                //Instaciando a Business
                Business_Cliente db = new Business_Cliente();

                //Camada do método Consultar passando DTO
                List<DTO_Cliente> list = db.Consultar(dto);

                //Desabilitando a geração automatica de colunas 
                dgvCliente.AutoGenerateColumns = false;

                //Passando o valor do método consultar para Grid
                dgvCliente.DataSource = list;


            }
            catch (Exception ex)
            {
                //Mensagem para o usuário
                MessageBox.Show("Ocorreu um erro ao consultar o cliente: " + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        Database_Cliente db = new Database_Cliente();
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                //Pegue o valor da GriedView 
                DTO_Cliente linha = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;

                //Mensagem de confirmação
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja alterar as infomações desse cliente?",
                                                      "ATENÇÃO!!",
                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //Verificação da resposta
                if (dialog == DialogResult.Yes)
                {
                    //Instanciamento da tela de alterar
                    frmAlterar_Cliente tela = new frmAlterar_Cliente();

                    //Chamada do Método LoadScreen
                    tela.LoadScreen(linha);

                    //Abrindo a tela de alterar
                    tela.Show();

                    //Oculto essa form
                    this.Hide();
                }
            }
            if (e.ColumnIndex == 4)
            {
                //Pegue o valor da GriedView 
                DTO_Cliente dto = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;

                //Mensagem de confirmação
                DialogResult r = MessageBox.Show("Tem certeza que deseja excluir esse cliente?",
                                                   "ATENÇÃO!!",
                                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //Verificação da resposta
                if (r == DialogResult.Yes)
                {
                    //Tela chama business
                    Business_Cliente db = new Business_Cliente();

                    //Passando o valor do ID a ser removido
                    db.Remover(dto.ID);

                    //Chamada do Método de carregamento de GridView
                    CarregarGrid();
                }

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //Chamada do método de Carregar Grid
            CarregarGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Ocultando a form
            this.Hide();

            //Instanciando a tela
            frmCadastro_Clientes tela = new frmCadastro_Clientes();

            //Fazendo a tela aparecer
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            //Ocultando a form
            this.Hide();

            //Instanciando a tela
            frmTela_Principal tela = new frmTela_Principal();

            //Fazendo a tela aparecer
            tela.Show();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            // Se apertar a tecla enter
            if (e.KeyData == Keys.Enter)
            {
                //Chamada do método CarregarGrid
                CarregarGrid();
            }
        }
    }
}

