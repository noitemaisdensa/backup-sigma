﻿using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Clientes.Tela;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Objetos.UF;
using Tela_principal.Objetos.Validadores;


namespace Tela_principal.Telas
{
    public partial class frmCadastro_Cliente : Form
    {
        public frmCadastro_Cliente()
        {
            InitializeComponent();

            List<string> carta = new List<string>();
            carta.Add("A");
            carta.Add("B");
            carta.Add("C");
            carta.Add("D");
            carta.Add("E");
            carta.Add("E");
            carta.Add("ACC");
            carta.Add("MOTOR-CASA");
            cboCarta.DataSource = carta;

            UF uf = new UF();
            cboUF.DataSource = uf.UFS();
        }
        Validacao validar = new Validacao();

        public void SalvarCliente()
        {
            try
            {
                DTO_Cliente dto = new DTO_Cliente();
                lblNumero.Text = dto.ID.ToString();
                dto.CPF = txtCPF.Text;
                dto.CNPJ = txtCNPJ.Text;
                dto.RG = txtRG.Text;

                if (rdnJuridica.Checked == true)
                {
                    if (validar.VerificarCNPJ(dto.CNPJ) == false)
                    {
                        MessageBox.Show("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                            return;
                    }
                }

                else
                {
                    if (validar.VerificarCPF(dto.CPF) == false)
                    {
                        MessageBox.Show("CPF inválido!" + "\n" + "Verifique se o CPF está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }


                    if (validar.VerificarRG(dto.RG) == false)
                    {
                        MessageBox.Show("RG inválido!" + "\n" + "Verifique se o RG está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }
                }
                if (dto.CNPJ == "  .   .   /    -")
                {
                    dto.CNPJ = null;
                }
                //Agora esta funfando...
                dto.Nome = txtNome.Text;
                dto.Profissao = txtProfissao.Text;
                dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = nudNumero.Value;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.Telefone_Residencial = txtTelefoneResidencial.Text;
                dto.ClienteDesde = DateTime.Now;

                if (chkF.Checked == true)
                {
                    //dto.Sexo = chkF.Checked; 
                    dto.Sexo = true;
                }
                else if (chkM.Checked == true)
                {
                    //dto.Sexo = chkM.Checked;
                    dto.Sexo = false;
                }

                Business_Cliente db = new Business_Cliente();
                bool existe = db.VerificarClientes(dto);
                if (existe == false)
                {
                    
                        db.Salvar(dto);
                        MessageBox.Show("Cliente cadastrado com sucesso!",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information);
                        lblNumero.Text = dto.ID.ToString();
                }
                else
                {
                    MessageBox.Show("Esse cliente já consta em nossa base de dados!",
                               "Sigma",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Desaparecer()
        {
            try
            {
                this.Hide();
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        public void Abracadabra()
        {

        }
        public void Alacazum()
        {
            try
            {

                DialogResult dialog = MessageBox.Show("Você realmente deseja deletar esse registro?",
                                "Sigma",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    DTO_Cliente dto = new DTO_Cliente();
                    int id = dto.ID;

                    Business_Cliente db = new Business_Cliente();
                    db.Remover(id);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }
        private void lblFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            SalvarCliente();
        }


        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Alacazum();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Desaparecer();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CorreiosApi service = new CorreiosApi();
                var cep = txtCEP.Text;
                var dados = service.consultaCEP(cep);

                string rua = dados.end;
                string bairro = dados.bairro;
                string cidade = dados.cidade;

                string endereco = rua + ", " + bairro + ", " + cidade;

                txtEndereco.Text = endereco;

                cboUF.SelectedItem = dados.uf;
            }
            catch (Exception)
            {

                MessageBox.Show("CEP invalido , digite novamente! \n",
                                "SIGMA",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SalvarCliente();
            }
        }

        private void rdnFisica_CheckedChanged(object sender, EventArgs e)
        {
            txtCNPJ.Enabled = false;
            txtCNPJ.Clear();

            txtCPF.Enabled = true;
            txtRG.Enabled = true;
            chkF.Enabled = true;
            chkM.Enabled = true;
            chkOutros.Enabled = true;
        }

        private void rdnJuridica_CheckedChanged(object sender, EventArgs e)
        {
            txtCNPJ.Enabled = true;

            txtCPF.Enabled = false;
            txtCPF.Clear();

            txtRG.Enabled = false;
            txtRG.Clear();

            chkF.Enabled = false;
            chkM.Enabled = false;
            chkOutros.Enabled = false;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmConsultar_Cliente tela = new frmConsultar_Cliente();
            tela.Show();
            this.Hide();

        }
    }
}
