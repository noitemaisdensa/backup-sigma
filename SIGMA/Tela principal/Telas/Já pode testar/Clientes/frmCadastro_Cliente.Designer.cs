﻿namespace Tela_principal.Telas
{
    partial class frmCadastro_Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastro_Clientes));
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.txtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtTelefoneResidencial = new System.Windows.Forms.MaskedTextBox();
            this.lbltxtTelefoneResidencial = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblCelular = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cboCarta = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFedelidade = new System.Windows.Forms.Label();
            this.lblFidelidade = new System.Windows.Forms.Label();
            this.lblNascimento = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.rdnJuridica = new System.Windows.Forms.RadioButton();
            this.rdnFisica = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(498, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(17, 17);
            this.lblFechar.TabIndex = 0;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtRG);
            this.panel1.Controls.Add(this.chkM);
            this.panel1.Controls.Add(this.chkF);
            this.panel1.Controls.Add(this.txtCNPJ);
            this.panel1.Controls.Add(this.txtCPF);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblCNPJ);
            this.panel1.Controls.Add(this.lblSexo);
            this.panel1.Controls.Add(this.lblCPF);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblNome);
            this.panel1.Controls.Add(this.lblNumero);
            this.panel1.Controls.Add(this.lblCodigo);
            this.panel1.Location = new System.Drawing.Point(13, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 159);
            this.panel1.TabIndex = 1;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(46, 111);
            this.txtRG.Mask = "99,999,999-9";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(143, 20);
            this.txtRG.TabIndex = 2;
            this.txtRG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkM.Location = new System.Drawing.Point(121, 140);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(34, 17);
            this.chkM.TabIndex = 5;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Checked = true;
            this.chkF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkF.Location = new System.Drawing.Point(74, 140);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(31, 17);
            this.chkF.TabIndex = 4;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Enabled = false;
            this.txtCNPJ.Location = new System.Drawing.Point(46, 62);
            this.txtCNPJ.Mask = "00,000,000/0000-00";
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(143, 20);
            this.txtCNPJ.TabIndex = 1;
            this.txtCNPJ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(46, 86);
            this.txtCPF.Mask = "000,000,000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(143, 20);
            this.txtCPF.TabIndex = 1;
            this.txtCPF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "RG:";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.Location = new System.Drawing.Point(2, 62);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(37, 13);
            this.lblCNPJ.TabIndex = 5;
            this.lblCNPJ.Text = "CNPJ:";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(3, 142);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(34, 13);
            this.lblSexo.TabIndex = 3;
            this.lblSexo.Text = "Sexo:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Location = new System.Drawing.Point(2, 86);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(30, 13);
            this.lblCPF.TabIndex = 5;
            this.lblCPF.Text = "CPF:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(47, 38);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(196, 20);
            this.txtNome.TabIndex = 0;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-4, -4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cliente";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(3, 41);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(38, 13);
            this.lblNome.TabIndex = 3;
            this.lblNome.Text = "Nome:";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Enabled = false;
            this.lblNumero.Location = new System.Drawing.Point(49, 22);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(10, 13);
            this.lblNumero.TabIndex = 0;
            this.lblNumero.Text = "-";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(3, 22);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(46, 13);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "Código :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.nudNumero);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.cboUF);
            this.panel2.Controls.Add(this.txtCEP);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.lblUF);
            this.panel2.Controls.Add(this.txtComplemento);
            this.panel2.Controls.Add(this.lblCompemento);
            this.panel2.Controls.Add(this.lblEndereco);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtEndereco);
            this.panel2.Controls.Add(this.lblCEP);
            this.panel2.Location = new System.Drawing.Point(13, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(502, 119);
            this.panel2.TabIndex = 2;
            // 
            // nudNumero
            // 
            this.nudNumero.Location = new System.Drawing.Point(440, 50);
            this.nudNumero.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(53, 20);
            this.nudNumero.TabIndex = 28;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Tela_principal.Properties.Resources.magnifier_glass_icon_icons_com_71148;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(195, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 23);
            this.button1.TabIndex = 8;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboUF
            // 
            this.cboUF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(69, 78);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(121, 21);
            this.cboUF.TabIndex = 9;
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(68, 52);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(121, 20);
            this.txtCEP.TabIndex = 7;
            this.txtCEP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(420, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "N°:";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Location = new System.Drawing.Point(38, 78);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(24, 13);
            this.lblUF.TabIndex = 25;
            this.lblUF.Text = "UF:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(353, 49);
            this.txtComplemento.MaxLength = 20;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(61, 20);
            this.txtComplemento.TabIndex = 10;
            this.txtComplemento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Location = new System.Drawing.Point(270, 52);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(77, 13);
            this.lblCompemento.TabIndex = 21;
            this.lblCompemento.Text = "Complemento :";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Location = new System.Drawing.Point(3, 26);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(59, 13);
            this.lblEndereco.TabIndex = 5;
            this.lblEndereco.Text = "Endereço :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(-1, -3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 22);
            this.label12.TabIndex = 5;
            this.label12.Text = "Endereço ";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(68, 23);
            this.txtEndereco.MaxLength = 70;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(427, 20);
            this.txtEndereco.TabIndex = 6;
            this.txtEndereco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Location = new System.Drawing.Point(28, 52);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(34, 13);
            this.lblCEP.TabIndex = 6;
            this.lblCEP.Text = "CEP :";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtEmail);
            this.panel3.Controls.Add(this.lblEmail);
            this.panel3.Controls.Add(this.txtTelefoneResidencial);
            this.panel3.Controls.Add(this.lbltxtTelefoneResidencial);
            this.panel3.Controls.Add(this.txtCelular);
            this.panel3.Controls.Add(this.lblCelular);
            this.panel3.Controls.Add(this.txtTelefone);
            this.panel3.Controls.Add(this.lblTelefone);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Location = new System.Drawing.Point(13, 344);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(264, 153);
            this.panel3.TabIndex = 3;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(46, 22);
            this.txtEmail.MaxLength = 70;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(196, 20);
            this.txtEmail.TabIndex = 17;
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(3, 25);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 33;
            this.lblEmail.Text = "Email:";
            // 
            // txtTelefoneResidencial
            // 
            this.txtTelefoneResidencial.Location = new System.Drawing.Point(68, 111);
            this.txtTelefoneResidencial.Mask = "(00)00000-0000";
            this.txtTelefoneResidencial.Name = "txtTelefoneResidencial";
            this.txtTelefoneResidencial.Size = new System.Drawing.Size(100, 20);
            this.txtTelefoneResidencial.TabIndex = 20;
            this.txtTelefoneResidencial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lbltxtTelefoneResidencial
            // 
            this.lbltxtTelefoneResidencial.AutoSize = true;
            this.lbltxtTelefoneResidencial.Location = new System.Drawing.Point(3, 111);
            this.lbltxtTelefoneResidencial.Name = "lbltxtTelefoneResidencial";
            this.lbltxtTelefoneResidencial.Size = new System.Drawing.Size(65, 26);
            this.lbltxtTelefoneResidencial.TabIndex = 31;
            this.lbltxtTelefoneResidencial.Text = "Telefone \r\nResidencial:";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(68, 81);
            this.txtCelular.Mask = "(99) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(100, 20);
            this.txtCelular.TabIndex = 19;
            this.txtCelular.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Location = new System.Drawing.Point(3, 84);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(42, 13);
            this.lblCelular.TabIndex = 29;
            this.lblCelular.Text = "Celular:";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(68, 53);
            this.txtTelefone.Mask = "(99) 00000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(100, 20);
            this.txtTelefone.TabIndex = 18;
            this.txtTelefone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Location = new System.Drawing.Point(3, 58);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(52, 13);
            this.lblTelefone.TabIndex = 27;
            this.lblTelefone.Text = "Telefone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Modern No. 20", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(-4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 22);
            this.label18.TabIndex = 27;
            this.label18.Text = "Contato";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Location = new System.Drawing.Point(404, 369);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 23);
            this.btnConsultar.TabIndex = 22;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Location = new System.Drawing.Point(300, 369);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 21;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Location = new System.Drawing.Point(300, 428);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 23;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(404, 428);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 24;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cboCarta);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.lblFedelidade);
            this.panel4.Controls.Add(this.lblFidelidade);
            this.panel4.Controls.Add(this.lblNascimento);
            this.panel4.Controls.Add(this.dtpNascimento);
            this.panel4.Controls.Add(this.txtProfissao);
            this.panel4.Controls.Add(this.lblProfissao);
            this.panel4.Controls.Add(this.rdnJuridica);
            this.panel4.Controls.Add(this.rdnFisica);
            this.panel4.Location = new System.Drawing.Point(262, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(253, 159);
            this.panel4.TabIndex = 2;
            // 
            // cboCarta
            // 
            this.cboCarta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboCarta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCarta.FormattingEnabled = true;
            this.cboCarta.Location = new System.Drawing.Point(70, 70);
            this.cboCarta.Name = "cboCarta";
            this.cboCarta.Size = new System.Drawing.Size(176, 21);
            this.cboCarta.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 26);
            this.label3.TabIndex = 31;
            this.label3.Text = "Tipo de \r\nCarta:\r\n";
            // 
            // lblFedelidade
            // 
            this.lblFedelidade.AutoSize = true;
            this.lblFedelidade.Enabled = false;
            this.lblFedelidade.Location = new System.Drawing.Point(97, 137);
            this.lblFedelidade.Name = "lblFedelidade";
            this.lblFedelidade.Size = new System.Drawing.Size(16, 13);
            this.lblFedelidade.TabIndex = 11;
            this.lblFedelidade.Text = "---";
            // 
            // lblFidelidade
            // 
            this.lblFidelidade.AutoSize = true;
            this.lblFidelidade.Location = new System.Drawing.Point(6, 137);
            this.lblFidelidade.Name = "lblFidelidade";
            this.lblFidelidade.Size = new System.Drawing.Size(77, 13);
            this.lblFidelidade.TabIndex = 30;
            this.lblFidelidade.Text = "Cliente desde :";
            // 
            // lblNascimento
            // 
            this.lblNascimento.AutoSize = true;
            this.lblNascimento.Location = new System.Drawing.Point(6, 107);
            this.lblNascimento.Name = "lblNascimento";
            this.lblNascimento.Size = new System.Drawing.Size(107, 13);
            this.lblNascimento.TabIndex = 29;
            this.lblNascimento.Text = "Data de Nascimento:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNascimento.Location = new System.Drawing.Point(112, 104);
            this.dtpNascimento.MaxDate = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.MinDate = new System.DateTime(1940, 1, 1, 0, 0, 0, 0);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(134, 20);
            this.dtpNascimento.TabIndex = 4;
            this.dtpNascimento.Value = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(70, 42);
            this.txtProfissao.MaxLength = 50;
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(176, 20);
            this.txtProfissao.TabIndex = 2;
            this.txtProfissao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Location = new System.Drawing.Point(6, 45);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(32, 13);
            this.lblProfissao.TabIndex = 28;
            this.lblProfissao.Text = "Área:";
            // 
            // rdnJuridica
            // 
            this.rdnJuridica.AutoSize = true;
            this.rdnJuridica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnJuridica.Location = new System.Drawing.Point(143, 13);
            this.rdnJuridica.Name = "rdnJuridica";
            this.rdnJuridica.Size = new System.Drawing.Size(101, 17);
            this.rdnJuridica.TabIndex = 1;
            this.rdnJuridica.Text = "Pessoa Jurídica";
            this.rdnJuridica.UseVisualStyleBackColor = true;
            this.rdnJuridica.CheckedChanged += new System.EventHandler(this.rdnJuridica_CheckedChanged);
            // 
            // rdnFisica
            // 
            this.rdnFisica.AutoSize = true;
            this.rdnFisica.Checked = true;
            this.rdnFisica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnFisica.Location = new System.Drawing.Point(45, 13);
            this.rdnFisica.Name = "rdnFisica";
            this.rdnFisica.Size = new System.Drawing.Size(92, 17);
            this.rdnFisica.TabIndex = 0;
            this.rdnFisica.TabStop = true;
            this.rdnFisica.Text = "Pessoa Física";
            this.rdnFisica.UseVisualStyleBackColor = true;
            this.rdnFisica.CheckedChanged += new System.EventHandler(this.rdnFisica_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Tela_principal.Properties.Resources.dwelling_house_318_1861;
            this.pictureBox1.Location = new System.Drawing.Point(473, 473);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(41, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frmCadastro_Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 509);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblFechar);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCadastro_Clientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_CadastroDeClientes";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label lblCompemento;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.MaskedTextBox txtTelefoneResidencial;
        private System.Windows.Forms.Label lbltxtTelefoneResidencial;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cboCarta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFedelidade;
        private System.Windows.Forms.Label lblFidelidade;
        private System.Windows.Forms.Label lblNascimento;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.TextBox txtProfissao;
        private System.Windows.Forms.Label lblProfissao;
        private System.Windows.Forms.RadioButton rdnJuridica;
        private System.Windows.Forms.RadioButton rdnFisica;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.MaskedTextBox txtCNPJ;
        private System.Windows.Forms.Label lblCNPJ;
        private System.Windows.Forms.NumericUpDown nudNumero;
    }
}