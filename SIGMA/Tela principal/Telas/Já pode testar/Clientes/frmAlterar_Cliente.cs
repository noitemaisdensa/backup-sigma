﻿using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Clientes.Tela;
using Tela_principal.Cadastro_de_Clientes;
using Tela_principal.Objetos.UF;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Telas.Clientes
{
    public partial class frmAlterar_Cliente : Form
    {
        public frmAlterar_Cliente()
        {
            InitializeComponent();
            UF uf = new UF();
            cboUF.DataSource = uf.UFS();

            List<string> carta = new List<string>();
            carta.Add("A");
            carta.Add("B");
            carta.Add("C");
            carta.Add("D");
            carta.Add("E");

            cboCarta.DataSource = carta;
        }

        //Validação, na verdade!
        Validacao validar = new Validacao();

        //Instanciando o DTO
        DTO_Cliente dto;

        //Olá, eu sou o Método LoadScreen!
        //E a minha função é passar os valores de outra tela para essa.
        public void LoadScreen(DTO_Cliente dto)
        {
            //Passando o valor que vem de outra tela para esse dto
            this.dto = dto;

            //Passando os valores para os controles
            lblNumero.Text = dto.ID.ToString();
            txtNome.Text = dto.Nome;
            txtCNPJ.Text = dto.CNPJ;
            txtCPF.Text = dto.CPF;
            txtRG.Text = dto.RG;
            txtEndereco.Text = dto.Endereco;
            txtCEP.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF.ToString();
            txtComplemento.Text = dto.Complemento;
            nudNumero.Value = dto.Numero;
            txtEmail.Text = dto.Email;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
            txtTelefoneResidencial.Text = dto.Telefone_Residencial;
            txtProfissao.Text = dto.Profissao;
            dtpNascimento.Value = dto.DatadeNascimento;
            cboCarta.SelectedItem = dto.Tipo_Carta;

            //Pessoa Juridica?
            if (rdnJuridica.Checked == true)
            {
                txtCNPJ.Text = dto.CNPJ;
            }

            //Pessoa Física?
            else
            {
                txtCPF.Text = dto.CPF;
                txtRG.Text = dto.RG;
            }

            //Mulher?
            if (dto.Sexo == true)
            {
                 chkF.Checked = true;
            }

            //Homem?
            else
            {
                chkM.Checked = true;
            }


        }

        private void rdnFisica_CheckedChanged(object sender, EventArgs e)
        {
            txtCNPJ.Enabled = false;
            txtCNPJ.Clear();

            txtCPF.Enabled = true;
            txtRG.Enabled = true;
            chkF.Enabled = true;
            chkM.Enabled = true;
        }

        private void rdnJuridica_CheckedChanged(object sender, EventArgs e)
        {
            txtCNPJ.Enabled = true;

            txtCPF.Enabled = false;
            txtCPF.Clear();

            txtRG.Enabled = false;
            txtRG.Clear();

            chkF.Enabled = false;
            chkM.Enabled = false;
        }

        public void AlterarCliente()
        {
            try
            {
                dto.CPF = txtCPF.Text;
                dto.CNPJ = txtCNPJ.Text;
                dto.RG = txtRG.Text;

                if (rdnJuridica.Checked == true)
                {
                    if (validar.VerificarCNPJ(dto.CNPJ) == false)
                    {
                        MessageBox.Show("CNPJ inválido!" + "\n" + "Verifique se o CNPJ está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }
                }

                else
                {
                    if (validar.VerificarCPF(dto.CPF) == false)
                    {
                        MessageBox.Show("CPF inválido!" + "\n" + "Verifique se o CPF está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }


                    if (validar.VerificarRG(dto.RG) == false)
                    {
                        MessageBox.Show("RG inválido!" + "\n" + "Verifique se o RG está digitalizado da maneira correta",
                                    "Sigma",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        return;
                    }
                }
                if (dto.CNPJ == "  .   .   /    -")
                {
                    dto.CNPJ = null;
                }
                //Agora esta funfando...
                dto.Nome = txtNome.Text;
                dto.Profissao = txtProfissao.Text;
                dto.Tipo_Carta = cboCarta.SelectedItem.ToString();
                dto.DatadeNascimento = Convert.ToDateTime(dtpNascimento.Value);
                dto.Endereco = txtEndereco.Text;
                dto.CEP = txtCEP.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Numero = nudNumero.Value;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.Telefone_Residencial = txtTelefoneResidencial.Text;


                if (chkF.Checked == true)
                {
                    dto.Sexo = true;
                }
                else 
                {
                    dto.Sexo = false;
                }
                

                Business_Cliente db = new Business_Cliente();
                db.Alterar(dto);
                MessageBox.Show("Dados do Cliente alterados com sucesso!",
                           "Sigma",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }
        

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlterarCliente();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AlterarCliente();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //Instacio o Objeto
                CorreiosApi service = new CorreiosApi();

                //Pegando o valor do CEP
                var cep = txtCEP.Text;

                //A partir do valor do Cep, a API retornará os valores
                var dados = service.consultaCEP(cep);

                //API retorna o nome da Rua
                string rua = dados.end;

                //API retorna o nome do Bairro
                string bairro = dados.bairro;

                //API retorna o nome da cidade
                string cidade = dados.cidade;

                //Concateno os valores acima
                string endereco = rua + ", " + bairro + ", " + cidade;

                //Passando o valor da variável para o controle
                txtEndereco.Text = endereco;

                //Passo o valor para a combobox;
                cboUF.SelectedItem = dados.uf;
            }
            catch (Exception)
            {
                //Mensagem para o usuário.
                MessageBox.Show("CEP invalido" + "\n" + "Digite novamente!",
                               "SIGMA",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Cliente tela = new frmConsultar_Cliente();
            tela.Show();
        }

    }
}
