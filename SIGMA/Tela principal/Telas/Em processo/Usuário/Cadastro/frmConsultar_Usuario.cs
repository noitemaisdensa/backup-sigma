﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Usuario;

namespace Tela_principal.Telas.Em_processo.Usuário.Cadastro
{
    public partial class frmConsultar_Usuario : Form
    {
        public frmConsultar_Usuario()
        {
            InitializeComponent();
            CarregarCombosGrid();
            CarregarPerfil();

        }
        public void CarregarPerfil()
        {
            List<string> perfil = new List<string>();
            perfil.Add("Selecione");
            perfil.Add("Administrador");
            perfil.Add("Funcionário");

            columnPerfil.DataSource = perfil;
        }
        public void CarregarCombosGrid()
        {
            Business_Departamento depart = new Business_Departamento();
            List<DTO_Departamento> lista = depart.Listar();

            columnDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            columnDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            columnDepartamento.DataSource = lista;
        }
        public void CarregarGrid()
        {
            try
            {
                DTO_Usuarios dto = new DTO_Usuarios();
                dto.Nome_User = txtNome.Text;

                Business_Usuario db = new Business_Usuario();
                List<DTO_Usuarios> consult = db.Consultar(dto);

                dgvUsuarios.AutoGenerateColumns = false;
                dgvUsuarios.DataSource = consult;

            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 4)
            {
                DTO_Usuarios linha = dgvUsuarios.CurrentRow.DataBoundItem as DTO_Usuarios;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja alterar esse usuário?",
                                                      "Sigma",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    this.Hide();
                    frmAlterar_Usuario tela = new frmAlterar_Usuario();
                    tela.LoadScreen(linha);
                    tela.Show();
                }
            }
            if (e.ColumnIndex == 5)
            {
                DTO_Usuarios dto = dgvUsuarios.CurrentRow.DataBoundItem as DTO_Usuarios;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja apagar esse usuário?",
                                                      "Sigma",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);
                if (true)
                {
                    Business_Usuario db = new Business_Usuario();
                    db.Remover(dto.ID);

                    CarregarGrid();
                }

            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastro_Usuario tela = new frmCadastro_Usuario();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }
    }
}
