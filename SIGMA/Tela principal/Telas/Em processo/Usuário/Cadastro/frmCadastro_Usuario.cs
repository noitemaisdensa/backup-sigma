﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Objetos.Validadores;
using Tela_principal.Telas.Em_processo.Usuário.Cadastro;

namespace Tela_principal
{
    public partial class frmCadastro_Usuario : Form
    {

        
        public frmCadastro_Usuario()
        {
            InitializeComponent();
            CarregarDepartamentos();
            CarregarPerfil();

            
        }
        public void CarregarPerfil()
        {
            List<string> perfil = new List<string>();
            perfil.Add("Selecione");
            perfil.Add("Administrador");
            perfil.Add("Funcionário");

            cboPerfil.DataSource = perfil;
        }

        public void CarregarDepartamentos()
        {
            Business_Departamento b = new Business_Departamento();
            List<DTO_Departamento> lista = b.Listar();

            cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            cboDepartamento.DataSource = lista;
        }

        public void VerificarSenha()
        {
            if (txtSenha.BackColor == Color.Red)
            {
                MessageBox.Show("Senha muito fraca, não esqueça de usar pelo menos:"
                                + "\n\n" + "Uma letra Maiúscula"
                                + "\n" + "Uma letra Minúscula"
                                + "\n" + "Um Número"
                                + "\n" + "Um Caractere Especial",
                                "Senha fraca",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                return;
            }
            if (txtSenha.Text != txtConfirmarSenha.Text)
            {
                MessageBox.Show("Senhas incorretas!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }
        }

        public void Automagicamente()
        {
            try
            {
                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                DTO_Usuarios dto = new DTO_Usuarios();
                dto.ID_Departamento = departamento.ID; //Relacionamento Completo!!
                dto.Nome_User = txtNome.Text;
                dto.Usuario = txtUsuario.Text;
                dto.Obeservacao = txtObservacao.Text;
                dto.Perfil = cboPerfil.SelectedItem.ToString();

                VerificarSenha();
                string senha = txtSenha.Text;
                SHA256Cript crip = new SHA256Cript();
                dto.Senha = crip.Criptografar(senha);

                if (rdnNao.Checked == true)
                {
                    dto.Checar = true;
                }
                else
                {
                    dto.Checar = false;
                }

                Business_Usuario db = new Business_Usuario();
                db.Salvar(dto);
                MessageBox.Show("Usuário salvo com sucesso",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }

        public void Magicamente()
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Usuario tela = new frmConsultar_Usuario();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Automagicamente();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (txtSenha.Text.Length > 0)
            //{
            //    pictureBox2.Image = Properties.Resources.lock_2;
            //}
            //
            //Validacao vali = new Validacao();
            //bool v = vali.Senha(txtSenha.Text);
            //if (v == true)
            //{
            //    this.txtSenha.BackColor = Color.LightGreen;
            //    pictureBox3.Image = Properties.Resources.lock_2;
            //}
            //else
            //{
            //    this.txtSenha.BackColor = Color.Red;
            //    pictureBox4.Image = Properties.Resources.lock_2;
            //}
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            if (txtSenha.Text.Length > 0)
            {
                pictureBox2.Image = Properties.Resources.lock_2;
            }

            Validacao vali = new Validacao();
            bool v = vali.Senha(txtSenha.Text);
            if (v == true)
            {
                this.txtSenha.BackColor = Color.LightGreen;
                pictureBox3.Image = Properties.Resources.lock_2;
            }
            else
            {
                this.txtSenha.BackColor = Color.Red;
                pictureBox4.Image = Properties.Resources.lock_2;
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Automagicamente();
            }
        }
    }
}
