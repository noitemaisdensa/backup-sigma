﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Objetos.Validadores;

namespace Tela_principal.Telas.Em_processo.Usuário.Cadastro
{
    public partial class frmAlterar_Usuario : Form
    {
        public frmAlterar_Usuario()
        {
            InitializeComponent();
            CarregarDepartamentos();
            CarregarPerfil();
        }
        DTO_Usuarios dto;

        public void LoadScreen(DTO_Usuarios dto)
        {
            this.dto = dto;

            cboDepartamento.SelectedItem = dto.ID_Departamento;
            txtNome.Text = dto.Nome_User;
            txtUsuario.Text = dto.Usuario;
            cboPerfil.SelectedItem = dto.Perfil;
            txtObservacao.Text = dto.Obeservacao;

            if (dto.Checar == true)
            {
                rdnNao.Checked = true;
            }
            else
            {
                rdnSim.Checked = true;
            }
        }
        public void CarregarPerfil()
        {
            List<string> perfil = new List<string>();
            perfil.Add("Selecione");
            perfil.Add("Administrador");
            perfil.Add("Funcionário");

            cboPerfil.DataSource = perfil;
        }
        public void CarregarDepartamentos()
        {
            Business_Departamento b = new Business_Departamento();
            List<DTO_Departamento> lista = b.Listar();

            cboDepartamento.ValueMember = nameof(DTO_Departamento.ID);
            cboDepartamento.DisplayMember = nameof(DTO_Departamento.Departamento);

            cboDepartamento.DataSource = lista;
        }

        public void VerificarSenha()
        {
            if (txtNovaSenha.BackColor == Color.Red)
            {
                MessageBox.Show("Senha muito fraca, não esqueça de usar pelo menos:"
                                + "\n\n" + "Uma letra Maiúscula"
                                + "\n" + "Uma letra Minúscula"
                                + "\n" + "Um Número"
                                + "\n" + "Um Caractere Especial",
                                "Senha fraca",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
                return;
            }
            if (txtNovaSenha.Text != txtConfirmarSenha.Text)
            {
                MessageBox.Show("Senhas incorretas!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }
        }

        public void AlterarUsuario()
        {
            try
            {
                DTO_Departamento departamento = cboDepartamento.SelectedItem as DTO_Departamento;

                dto.ID_Departamento = departamento.ID; //Relacionamento Completo!!
                dto.Nome_User = txtNome.Text;
                dto.Usuario = txtUsuario.Text;
                dto.Obeservacao = txtObservacao.Text;
                dto.Perfil = cboPerfil.SelectedItem.ToString();

                VerificarSenha();
                string senha = txtSenha.Text;
                SHA256Cript crip = new SHA256Cript();
                dto.Senha = crip.Criptografar(senha);

                if (rdnNao.Checked == true)
                {
                    dto.Checar = true;
                }
                else
                {
                    dto.Checar = false;
                }



                Business_Usuario db = new Business_Usuario();
                db.Alterar(dto);
                MessageBox.Show("Usuário alterado com sucesso",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("ERRO!" + "\n" + ex.Message,
                                 "Sigma",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
            catch (Exception erro)
            {
                MessageBox.Show("ERRO!" + "\n" + erro.Message,
                                  "Sigma",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Error);
            }
        }
        private void btnAlterar_Click(object sender, EventArgs e)
        {
            AlterarUsuario();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultar_Usuario tela = new frmConsultar_Usuario();
            tela.Show();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AlterarUsuario();
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void txtNovaSenha_Leave(object sender, EventArgs e)
        {
            if (txtNovaSenha.Text.Length > 0)
            {
                pictureBox2.Image = Properties.Resources.lock_2;
            }

            Validacao vali = new Validacao();
            bool v = vali.Senha(txtNovaSenha.Text);
            if (v == true)
            {
                this.txtNovaSenha.BackColor = Color.LightGreen;
                pictureBox3.Image = Properties.Resources.lock_2;
            }
            else
            {
                this.txtNovaSenha.BackColor = Color.Red;
                pictureBox4.Image = Properties.Resources.lock_2;
            }
        }
    }
}
