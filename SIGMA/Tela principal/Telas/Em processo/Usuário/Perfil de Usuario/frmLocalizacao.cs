﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tela_principal.Cadastro_Usuario.Perfil_de_Usuario
{
    public partial class frmLocalizacao : Form
    {
        public frmLocalizacao()
        {
            InitializeComponent();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            DTO_Usuarios dto = new DTO_Usuarios();
            dto.Nome_User = txtFuncionario.Text;

            Business_Usuario db = new Business_Usuario();
            List<DTO_Usuarios> list = db.Consultar(dto);

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = list;

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmPerfil_Usuarios tela = new frmPerfil_Usuarios();
            tela.Show();
        }
    }
}
