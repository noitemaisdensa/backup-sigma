﻿namespace Tela_principal
{
    partial class frmPerfil_Usuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPerfil_Usuarios));
            this.lblFunfa = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblProdutos = new System.Windows.Forms.Label();
            this.lblVeiculos = new System.Windows.Forms.Label();
            this.lblFornecedores = new System.Windows.Forms.Label();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblServicos = new System.Windows.Forms.Label();
            this.chkAcessorCliente = new System.Windows.Forms.CheckBox();
            this.chkInserirClientes = new System.Windows.Forms.CheckBox();
            this.chkAlterarCliente = new System.Windows.Forms.CheckBox();
            this.chkExcluirCliente = new System.Windows.Forms.CheckBox();
            this.chkExcluirFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkInserirFuncionarios = new System.Windows.Forms.CheckBox();
            this.chkAcessoFuncionario = new System.Windows.Forms.CheckBox();
            this.chkExcluirFornecedores = new System.Windows.Forms.CheckBox();
            this.chkAlterarFornecedores = new System.Windows.Forms.CheckBox();
            this.chkInserirFornecedores = new System.Windows.Forms.CheckBox();
            this.chkAcessoFornecedores = new System.Windows.Forms.CheckBox();
            this.chkExcluirVeiculos = new System.Windows.Forms.CheckBox();
            this.chkAlterarVeiculos = new System.Windows.Forms.CheckBox();
            this.chkInserirVeiculos = new System.Windows.Forms.CheckBox();
            this.chkAcessoVeiculos = new System.Windows.Forms.CheckBox();
            this.chkExcluirProdutos = new System.Windows.Forms.CheckBox();
            this.chkAlterarProdutos = new System.Windows.Forms.CheckBox();
            this.chkInserirProdutos = new System.Windows.Forms.CheckBox();
            this.chkAcessoProdutos = new System.Windows.Forms.CheckBox();
            this.chkExcluirServicos = new System.Windows.Forms.CheckBox();
            this.chkAlterarServicos = new System.Windows.Forms.CheckBox();
            this.chkInserirServicos = new System.Windows.Forms.CheckBox();
            this.chkAcessoServicos = new System.Windows.Forms.CheckBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnLocalizar = new System.Windows.Forms.Button();
            this.lblFechar = new System.Windows.Forms.Label();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFunfa
            // 
            this.lblFunfa.AutoSize = true;
            this.lblFunfa.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunfa.Location = new System.Drawing.Point(26, 38);
            this.lblFunfa.Name = "lblFunfa";
            this.lblFunfa.Size = new System.Drawing.Size(103, 18);
            this.lblFunfa.TabIndex = 11;
            this.lblFunfa.Text = "Funcionário: ";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(83, 88);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(67, 18);
            this.lblCliente.TabIndex = 12;
            this.lblCliente.Text = "Clientes";
            // 
            // lblProdutos
            // 
            this.lblProdutos.AutoSize = true;
            this.lblProdutos.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdutos.Location = new System.Drawing.Point(78, 217);
            this.lblProdutos.Name = "lblProdutos";
            this.lblProdutos.Size = new System.Drawing.Size(70, 18);
            this.lblProdutos.TabIndex = 13;
            this.lblProdutos.Text = "Produtos";
            // 
            // lblVeiculos
            // 
            this.lblVeiculos.AutoSize = true;
            this.lblVeiculos.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVeiculos.Location = new System.Drawing.Point(78, 183);
            this.lblVeiculos.Name = "lblVeiculos";
            this.lblVeiculos.Size = new System.Drawing.Size(72, 18);
            this.lblVeiculos.TabIndex = 14;
            this.lblVeiculos.Text = "Veículos";
            // 
            // lblFornecedores
            // 
            this.lblFornecedores.AutoSize = true;
            this.lblFornecedores.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedores.Location = new System.Drawing.Point(40, 150);
            this.lblFornecedores.Name = "lblFornecedores";
            this.lblFornecedores.Size = new System.Drawing.Size(110, 18);
            this.lblFornecedores.TabIndex = 15;
            this.lblFornecedores.Text = "Fornecedores";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.Location = new System.Drawing.Point(43, 118);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(101, 18);
            this.lblFuncionario.TabIndex = 16;
            this.lblFuncionario.Text = "Funcionários";
            // 
            // lblServicos
            // 
            this.lblServicos.AutoSize = true;
            this.lblServicos.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicos.Location = new System.Drawing.Point(78, 251);
            this.lblServicos.Name = "lblServicos";
            this.lblServicos.Size = new System.Drawing.Size(69, 18);
            this.lblServicos.TabIndex = 17;
            this.lblServicos.Text = "Serviços";
            // 
            // chkAcessorCliente
            // 
            this.chkAcessorCliente.AutoSize = true;
            this.chkAcessorCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessorCliente.Location = new System.Drawing.Point(150, 91);
            this.chkAcessorCliente.Name = "chkAcessorCliente";
            this.chkAcessorCliente.Size = new System.Drawing.Size(105, 17);
            this.chkAcessorCliente.TabIndex = 18;
            this.chkAcessorCliente.Text = "Acesso ao menu";
            this.chkAcessorCliente.UseVisualStyleBackColor = true;
            // 
            // chkInserirClientes
            // 
            this.chkInserirClientes.AutoSize = true;
            this.chkInserirClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirClientes.Location = new System.Drawing.Point(255, 91);
            this.chkInserirClientes.Name = "chkInserirClientes";
            this.chkInserirClientes.Size = new System.Drawing.Size(86, 17);
            this.chkInserirClientes.TabIndex = 19;
            this.chkInserirClientes.Text = "Inserir dados";
            this.chkInserirClientes.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCliente
            // 
            this.chkAlterarCliente.AutoSize = true;
            this.chkAlterarCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCliente.Location = new System.Drawing.Point(347, 91);
            this.chkAlterarCliente.Name = "chkAlterarCliente";
            this.chkAlterarCliente.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarCliente.TabIndex = 20;
            this.chkAlterarCliente.Text = "Alterar dados";
            this.chkAlterarCliente.UseVisualStyleBackColor = true;
            // 
            // chkExcluirCliente
            // 
            this.chkExcluirCliente.AutoSize = true;
            this.chkExcluirCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirCliente.Location = new System.Drawing.Point(433, 91);
            this.chkExcluirCliente.Name = "chkExcluirCliente";
            this.chkExcluirCliente.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirCliente.TabIndex = 21;
            this.chkExcluirCliente.Text = "Excluir dados";
            this.chkExcluirCliente.UseVisualStyleBackColor = true;
            // 
            // chkExcluirFuncionario
            // 
            this.chkExcluirFuncionario.AutoSize = true;
            this.chkExcluirFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirFuncionario.Location = new System.Drawing.Point(433, 121);
            this.chkExcluirFuncionario.Name = "chkExcluirFuncionario";
            this.chkExcluirFuncionario.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirFuncionario.TabIndex = 25;
            this.chkExcluirFuncionario.Text = "Excluir dados";
            this.chkExcluirFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(347, 121);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarFuncionario.TabIndex = 24;
            this.chkAlterarFuncionario.Text = "Alterar dados";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkInserirFuncionarios
            // 
            this.chkInserirFuncionarios.AutoSize = true;
            this.chkInserirFuncionarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirFuncionarios.Location = new System.Drawing.Point(255, 121);
            this.chkInserirFuncionarios.Name = "chkInserirFuncionarios";
            this.chkInserirFuncionarios.Size = new System.Drawing.Size(86, 17);
            this.chkInserirFuncionarios.TabIndex = 23;
            this.chkInserirFuncionarios.Text = "Inserir dados";
            this.chkInserirFuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkAcessoFuncionario
            // 
            this.chkAcessoFuncionario.AutoSize = true;
            this.chkAcessoFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessoFuncionario.Location = new System.Drawing.Point(150, 121);
            this.chkAcessoFuncionario.Name = "chkAcessoFuncionario";
            this.chkAcessoFuncionario.Size = new System.Drawing.Size(105, 17);
            this.chkAcessoFuncionario.TabIndex = 22;
            this.chkAcessoFuncionario.Text = "Acesso ao menu";
            this.chkAcessoFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkExcluirFornecedores
            // 
            this.chkExcluirFornecedores.AutoSize = true;
            this.chkExcluirFornecedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirFornecedores.Location = new System.Drawing.Point(433, 153);
            this.chkExcluirFornecedores.Name = "chkExcluirFornecedores";
            this.chkExcluirFornecedores.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirFornecedores.TabIndex = 29;
            this.chkExcluirFornecedores.Text = "Excluir dados";
            this.chkExcluirFornecedores.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFornecedores
            // 
            this.chkAlterarFornecedores.AutoSize = true;
            this.chkAlterarFornecedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFornecedores.Location = new System.Drawing.Point(347, 153);
            this.chkAlterarFornecedores.Name = "chkAlterarFornecedores";
            this.chkAlterarFornecedores.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarFornecedores.TabIndex = 28;
            this.chkAlterarFornecedores.Text = "Alterar dados";
            this.chkAlterarFornecedores.UseVisualStyleBackColor = true;
            // 
            // chkInserirFornecedores
            // 
            this.chkInserirFornecedores.AutoSize = true;
            this.chkInserirFornecedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirFornecedores.Location = new System.Drawing.Point(255, 153);
            this.chkInserirFornecedores.Name = "chkInserirFornecedores";
            this.chkInserirFornecedores.Size = new System.Drawing.Size(86, 17);
            this.chkInserirFornecedores.TabIndex = 27;
            this.chkInserirFornecedores.Text = "Inserir dados";
            this.chkInserirFornecedores.UseVisualStyleBackColor = true;
            // 
            // chkAcessoFornecedores
            // 
            this.chkAcessoFornecedores.AutoSize = true;
            this.chkAcessoFornecedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessoFornecedores.Location = new System.Drawing.Point(150, 153);
            this.chkAcessoFornecedores.Name = "chkAcessoFornecedores";
            this.chkAcessoFornecedores.Size = new System.Drawing.Size(105, 17);
            this.chkAcessoFornecedores.TabIndex = 26;
            this.chkAcessoFornecedores.Text = "Acesso ao menu";
            this.chkAcessoFornecedores.UseVisualStyleBackColor = true;
            // 
            // chkExcluirVeiculos
            // 
            this.chkExcluirVeiculos.AutoSize = true;
            this.chkExcluirVeiculos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirVeiculos.Location = new System.Drawing.Point(433, 183);
            this.chkExcluirVeiculos.Name = "chkExcluirVeiculos";
            this.chkExcluirVeiculos.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirVeiculos.TabIndex = 33;
            this.chkExcluirVeiculos.Text = "Excluir dados";
            this.chkExcluirVeiculos.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVeiculos
            // 
            this.chkAlterarVeiculos.AutoSize = true;
            this.chkAlterarVeiculos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVeiculos.Location = new System.Drawing.Point(347, 183);
            this.chkAlterarVeiculos.Name = "chkAlterarVeiculos";
            this.chkAlterarVeiculos.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarVeiculos.TabIndex = 32;
            this.chkAlterarVeiculos.Text = "Alterar dados";
            this.chkAlterarVeiculos.UseVisualStyleBackColor = true;
            // 
            // chkInserirVeiculos
            // 
            this.chkInserirVeiculos.AutoSize = true;
            this.chkInserirVeiculos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirVeiculos.Location = new System.Drawing.Point(255, 183);
            this.chkInserirVeiculos.Name = "chkInserirVeiculos";
            this.chkInserirVeiculos.Size = new System.Drawing.Size(86, 17);
            this.chkInserirVeiculos.TabIndex = 31;
            this.chkInserirVeiculos.Text = "Inserir dados";
            this.chkInserirVeiculos.UseVisualStyleBackColor = true;
            // 
            // chkAcessoVeiculos
            // 
            this.chkAcessoVeiculos.AutoSize = true;
            this.chkAcessoVeiculos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessoVeiculos.Location = new System.Drawing.Point(150, 183);
            this.chkAcessoVeiculos.Name = "chkAcessoVeiculos";
            this.chkAcessoVeiculos.Size = new System.Drawing.Size(105, 17);
            this.chkAcessoVeiculos.TabIndex = 30;
            this.chkAcessoVeiculos.Text = "Acesso ao menu";
            this.chkAcessoVeiculos.UseVisualStyleBackColor = true;
            // 
            // chkExcluirProdutos
            // 
            this.chkExcluirProdutos.AutoSize = true;
            this.chkExcluirProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirProdutos.Location = new System.Drawing.Point(433, 217);
            this.chkExcluirProdutos.Name = "chkExcluirProdutos";
            this.chkExcluirProdutos.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirProdutos.TabIndex = 37;
            this.chkExcluirProdutos.Text = "Excluir dados";
            this.chkExcluirProdutos.UseVisualStyleBackColor = true;
            // 
            // chkAlterarProdutos
            // 
            this.chkAlterarProdutos.AutoSize = true;
            this.chkAlterarProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarProdutos.Location = new System.Drawing.Point(347, 217);
            this.chkAlterarProdutos.Name = "chkAlterarProdutos";
            this.chkAlterarProdutos.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarProdutos.TabIndex = 36;
            this.chkAlterarProdutos.Text = "Alterar dados";
            this.chkAlterarProdutos.UseVisualStyleBackColor = true;
            // 
            // chkInserirProdutos
            // 
            this.chkInserirProdutos.AutoSize = true;
            this.chkInserirProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirProdutos.Location = new System.Drawing.Point(255, 217);
            this.chkInserirProdutos.Name = "chkInserirProdutos";
            this.chkInserirProdutos.Size = new System.Drawing.Size(86, 17);
            this.chkInserirProdutos.TabIndex = 35;
            this.chkInserirProdutos.Text = "Inserir dados";
            this.chkInserirProdutos.UseVisualStyleBackColor = true;
            // 
            // chkAcessoProdutos
            // 
            this.chkAcessoProdutos.AutoSize = true;
            this.chkAcessoProdutos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessoProdutos.Location = new System.Drawing.Point(150, 217);
            this.chkAcessoProdutos.Name = "chkAcessoProdutos";
            this.chkAcessoProdutos.Size = new System.Drawing.Size(105, 17);
            this.chkAcessoProdutos.TabIndex = 34;
            this.chkAcessoProdutos.Text = "Acesso ao menu";
            this.chkAcessoProdutos.UseVisualStyleBackColor = true;
            // 
            // chkExcluirServicos
            // 
            this.chkExcluirServicos.AutoSize = true;
            this.chkExcluirServicos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkExcluirServicos.Location = new System.Drawing.Point(433, 251);
            this.chkExcluirServicos.Name = "chkExcluirServicos";
            this.chkExcluirServicos.Size = new System.Drawing.Size(89, 17);
            this.chkExcluirServicos.TabIndex = 41;
            this.chkExcluirServicos.Text = "Excluir dados";
            this.chkExcluirServicos.UseVisualStyleBackColor = true;
            // 
            // chkAlterarServicos
            // 
            this.chkAlterarServicos.AutoSize = true;
            this.chkAlterarServicos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarServicos.Location = new System.Drawing.Point(347, 251);
            this.chkAlterarServicos.Name = "chkAlterarServicos";
            this.chkAlterarServicos.Size = new System.Drawing.Size(88, 17);
            this.chkAlterarServicos.TabIndex = 40;
            this.chkAlterarServicos.Text = "Alterar dados";
            this.chkAlterarServicos.UseVisualStyleBackColor = true;
            // 
            // chkInserirServicos
            // 
            this.chkInserirServicos.AutoSize = true;
            this.chkInserirServicos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkInserirServicos.Location = new System.Drawing.Point(255, 251);
            this.chkInserirServicos.Name = "chkInserirServicos";
            this.chkInserirServicos.Size = new System.Drawing.Size(86, 17);
            this.chkInserirServicos.TabIndex = 39;
            this.chkInserirServicos.Text = "Inserir dados";
            this.chkInserirServicos.UseVisualStyleBackColor = true;
            // 
            // chkAcessoServicos
            // 
            this.chkAcessoServicos.AutoSize = true;
            this.chkAcessoServicos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAcessoServicos.Location = new System.Drawing.Point(150, 251);
            this.chkAcessoServicos.Name = "chkAcessoServicos";
            this.chkAcessoServicos.Size = new System.Drawing.Size(105, 17);
            this.chkAcessoServicos.TabIndex = 38;
            this.chkAcessoServicos.Text = "Acesso ao menu";
            this.chkAcessoServicos.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Location = new System.Drawing.Point(275, 323);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 43;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Location = new System.Drawing.Point(437, 323);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 42;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Location = new System.Drawing.Point(194, 323);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 47;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Location = new System.Drawing.Point(113, 323);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 46;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnAlterar
            // 
            this.btnAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Location = new System.Drawing.Point(32, 323);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 23);
            this.btnAlterar.TabIndex = 45;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLocalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocalizar.Location = new System.Drawing.Point(12, 12);
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(75, 23);
            this.btnLocalizar.TabIndex = 48;
            this.btnLocalizar.Text = "Localizar";
            this.btnLocalizar.UseVisualStyleBackColor = true;
            this.btnLocalizar.Click += new System.EventHandler(this.btnLocalizar_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(525, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(21, 20);
            this.lblFechar.TabIndex = 49;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(135, 39);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(377, 21);
            this.cboFuncionario.TabIndex = 57;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Tela_principal.Properties.Resources.dwelling_house_318_1861;
            this.pictureBox1.Location = new System.Drawing.Point(518, 322);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frmPerfil_Usuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 358);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.btnLocalizar);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.chkExcluirServicos);
            this.Controls.Add(this.chkAlterarServicos);
            this.Controls.Add(this.chkInserirServicos);
            this.Controls.Add(this.chkAcessoServicos);
            this.Controls.Add(this.chkExcluirProdutos);
            this.Controls.Add(this.chkAlterarProdutos);
            this.Controls.Add(this.chkInserirProdutos);
            this.Controls.Add(this.chkAcessoProdutos);
            this.Controls.Add(this.chkExcluirVeiculos);
            this.Controls.Add(this.chkAlterarVeiculos);
            this.Controls.Add(this.chkInserirVeiculos);
            this.Controls.Add(this.chkAcessoVeiculos);
            this.Controls.Add(this.chkExcluirFornecedores);
            this.Controls.Add(this.chkAlterarFornecedores);
            this.Controls.Add(this.chkInserirFornecedores);
            this.Controls.Add(this.chkAcessoFornecedores);
            this.Controls.Add(this.chkExcluirFuncionario);
            this.Controls.Add(this.chkAlterarFuncionario);
            this.Controls.Add(this.chkInserirFuncionarios);
            this.Controls.Add(this.chkAcessoFuncionario);
            this.Controls.Add(this.chkExcluirCliente);
            this.Controls.Add(this.chkAlterarCliente);
            this.Controls.Add(this.chkInserirClientes);
            this.Controls.Add(this.chkAcessorCliente);
            this.Controls.Add(this.lblServicos);
            this.Controls.Add(this.lblFuncionario);
            this.Controls.Add(this.lblFornecedores);
            this.Controls.Add(this.lblVeiculos);
            this.Controls.Add(this.lblProdutos);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblFunfa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPerfil_Usuarios";
            this.Text = "PerfildeUsuarios";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunfa;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblProdutos;
        private System.Windows.Forms.Label lblVeiculos;
        private System.Windows.Forms.Label lblFornecedores;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.Label lblServicos;
        private System.Windows.Forms.CheckBox chkAcessorCliente;
        private System.Windows.Forms.CheckBox chkInserirClientes;
        private System.Windows.Forms.CheckBox chkAlterarCliente;
        private System.Windows.Forms.CheckBox chkExcluirCliente;
        private System.Windows.Forms.CheckBox chkExcluirFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkInserirFuncionarios;
        private System.Windows.Forms.CheckBox chkAcessoFuncionario;
        private System.Windows.Forms.CheckBox chkExcluirFornecedores;
        private System.Windows.Forms.CheckBox chkAlterarFornecedores;
        private System.Windows.Forms.CheckBox chkInserirFornecedores;
        private System.Windows.Forms.CheckBox chkAcessoFornecedores;
        private System.Windows.Forms.CheckBox chkExcluirVeiculos;
        private System.Windows.Forms.CheckBox chkAlterarVeiculos;
        private System.Windows.Forms.CheckBox chkInserirVeiculos;
        private System.Windows.Forms.CheckBox chkAcessoVeiculos;
        private System.Windows.Forms.CheckBox chkExcluirProdutos;
        private System.Windows.Forms.CheckBox chkAlterarProdutos;
        private System.Windows.Forms.CheckBox chkInserirProdutos;
        private System.Windows.Forms.CheckBox chkAcessoProdutos;
        private System.Windows.Forms.CheckBox chkExcluirServicos;
        private System.Windows.Forms.CheckBox chkAlterarServicos;
        private System.Windows.Forms.CheckBox chkInserirServicos;
        private System.Windows.Forms.CheckBox chkAcessoServicos;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnLocalizar;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cboFuncionario;
    }
}