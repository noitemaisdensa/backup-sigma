﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_veiculos;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Cadastro_Usuario.Perfil_de_Usuario;
using Tela_principal.Login;
using Tela_principal.Orçamento;
using Tela_principal.Ordem_de_Servico;
using Tela_principal.Sub_Telas;

namespace Tela_principal
{
    public partial class frmPerfil_Usuarios : Form
    {
        public frmPerfil_Usuarios()
        {
            InitializeComponent();
            CarregarCombos();
        }

        public static string NivelAcesso;

        public void CarregarCombos ()
        {
            Business_Usuario db = new Business_Usuario();
            List<DTO_Usuarios> lista = db.Listar();

            cboFuncionario.ValueMember = nameof(DTO_Usuarios.ID);
            cboFuncionario.DisplayMember = nameof(DTO_Usuarios.Nome_User);

            cboFuncionario.DataSource = lista;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
                this.Hide();
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLocalizacao tela = new frmLocalizacao();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            DTO_Usuarios user = cboFuncionario.SelectedItem as DTO_Usuarios;
            DTO_Acesso dto = new DTO_Acesso();

            dto.ID_Usuario = user.ID; 


            if (chkAcessoVeiculos.Checked == true)
            {
                dto.Acesso_Menu = chkAcessoVeiculos.Checked;
            }
            if (chkAcessorCliente.Checked == true)
            {
                dto.Acesso_Menu = chkAcessorCliente.Checked;
            }
            if (chkAcessoFornecedores.Checked == true)
            {
                dto.Acesso_Menu = chkAcessoFornecedores.Checked;
            }
            if (chkAcessoProdutos.Checked == true)
            {
                dto.Acesso_Menu = chkAcessoProdutos.Checked;
            }
            if (chkAcessoFuncionario.Checked == true)
            {
                dto.Acesso_Menu = chkAcessoFuncionario.Checked;
            }
            if (chkAcessoServicos.Checked == true)
            {
                dto.Acesso_Menu = chkAcessoServicos.Checked;
            }
            if (chkAlterarCliente.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarCliente.Checked;
            }
            if (chkAlterarFornecedores.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarFornecedores.Checked;
            }
            if (chkAlterarFuncionario.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarFuncionario.Checked;
            }
            if (chkAlterarProdutos.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarProdutos.Checked;
            }
            if (chkAlterarServicos.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarServicos.Checked;
            }
            if (chkAlterarVeiculos.Checked == true)
            {
                dto.Alterar_Dados = chkAlterarVeiculos.Checked;
            }
            if (chkExcluirCliente.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirCliente.Checked;
            }
            if (chkExcluirFornecedores.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirFornecedores.Checked;
            }
            if (chkExcluirFuncionario.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirFuncionario.Checked;
            }
            if (chkExcluirProdutos.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirProdutos.Checked;
            }
            if (chkExcluirServicos.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirServicos.Checked;
            }
            if (chkExcluirVeiculos.Checked == true)
            {
                dto.Excluir_Dados = chkExcluirVeiculos.Checked;
            }
            if (chkInserirClientes.Checked == true)
            {
                dto.Inserir_Dados = chkInserirClientes.Checked;
            }
            if (chkInserirFornecedores.Checked == true)
            {
                dto.Inserir_Dados = chkInserirFornecedores.Checked;
            }
            if (chkInserirFuncionarios.Checked == true)
            {
                dto.Inserir_Dados = chkInserirFuncionarios.Checked;
            }
            if (chkInserirProdutos.Checked == true)
            {
                dto.Inserir_Dados = chkInserirProdutos.Checked;
            }
            if (chkInserirServicos.Checked == true)
            {
                dto.Inserir_Dados = chkInserirServicos.Checked;
            }
            if (chkInserirVeiculos.Checked == true)
            {
                dto.Inserir_Dados = chkInserirVeiculos.Checked;
            }

            Business_Acesso db = new Business_Acesso();
            db.Salvar(dto);

            MessageBox.Show("Permissões atribuidas com sucesso", 
                            "Sigma",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

        }
    }
}
