﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Tela_principal.Telas;
using Tela_principal.Orçamento;
using Tela_principal.Cadastro_de_veiculos;
using Tela_principal.Sub_Telas;
using Tela_principal.CadastrarFabricantes;

namespace Tela_principal
{
    public partial class frmTela_Principal : Form
    {
        public frmTela_Principal()
        {
            InitializeComponent();
            //VerificarPermissoes();
        }

        public void VerificarPermissoes()
        {
            //if (UserSession.UsuarioLogado.Acesso_Menu == false)
            //{
            //
            //    if (UserSession.UsuarioLogado.Inserir_Dados == false)
            //    {
            //        ClientelStripMenuItem.Enabled = false;
            //        btnClientes.Enabled = false;
            //        btnFornecedores.Enabled = false;
            //        btnFuncionarios.Enabled = false;
            //        btnProdutos.Enabled = false;
            //        btnVeiculos.Enabled = false;
            //    }
            //
            //    if (UserSession.UsuarioLogado.Alterar_Dados == false)
            //    {
            //        ClientelStripMenuItem.Enabled = false;
            //        RelatoriosStripMenuItem.Enabled = false;
            //        btnClientes.Enabled = false;
            //        btnFornecedores.Enabled = false;
            //        btnFuncionarios.Enabled = false;
            //        btnProdutos.Enabled = false;
            //        btnVeiculos.Enabled = false;
            //    }
            //
            //    if (UserSession.UsuarioLogado.Excluir_Dados == false)
            //    {
            //        ClientelStripMenuItem.Enabled = false;
            //        RelatoriosStripMenuItem.Enabled = false;
            //        MovimentoStripMenuItem.Enabled = false;
            //        btnClientes.Enabled = false;
            //        btnFornecedores.Enabled = false;
            //        btnFuncionarios.Enabled = false;
            //        btnProdutos.Enabled = false;
            //        btnVeiculos.Enabled = false;
            //    }
            //}
        }
        private void label1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar está tela?",
                                                  "Projeto Sigma",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                this.Close();
                Application.Exit();

            }
        }

        private void utilitáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IniciarCalculadora();
        }

        private void IniciarCalculadora()
        {
            frmCalculadora calc = new frmCalculadora();

            //Iniciava a calculadora do Windowns
            //Process.Start("Calc.exe");
        }

        private void btnCalculadora_Click(object sender, EventArgs e)
        {
            frmCalculadora calc = new frmCalculadora();
            //Process.Start("Calc.exe");
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastro_Clientes tela = new frmCadastro_Clientes();
            tela.Show();

        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Fabricantes tela = new frmCadastrar_Fabricantes();
            tela.Show();
            this.Close();

        }

        private void funcionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Funcionario tela = new frmCadastrar_Funcionario();
            tela.Show();
            this.Close();
        }

        private void veículosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastro_Veiculos tela = new frmCadastro_Veiculos();
            tela.Show();
            this.Close();
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void serviçosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void marcasEModelosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void logoffToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            frmCadastro_Clientes tela = new frmCadastro_Clientes();
            tela.Show();
            this.Close();

        }

        private void btnFornecedores_Click(object sender, EventArgs e)
        {

        }

        private void btnFuncionarios_Click(object sender, EventArgs e)
        {
            frmCadastrar_Funcionario tela = new frmCadastrar_Funcionario();
            tela.Show();
            this.Close();
        }

        private void btnVeiculos_Click(object sender, EventArgs e)
        {
            frmCadastro_Veiculos tela = new frmCadastro_Veiculos();
            tela.Show();
            this.Close();
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            frmOrcamento tela = new frmOrcamento();
            tela.Show();
            this.Close();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            LoginUsuario tela = new LoginUsuario();
            tela.Show();
            this.Close();
        }

        private void Cad_Click(object sender, EventArgs e)
        {
            frmCadastro_Usuario tela = new frmCadastro_Usuario();
            tela.Show();
            this.Close();
        }

        private void btnProdutos_Click(object sender, EventArgs e)
        {

        }

        private void RelatoriosStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Erro \n verssão de testes não possui essa funcionalidade",
                            "ERRO",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
        }

        private void MovimentoStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Erro \n verssão de testes não possui essa funcionalidade",
            // "ERRO",
            // MessageBoxButtons.OK,
            // MessageBoxIcon.Error);
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void btnCalendario_Click(object sender, EventArgs e)
        {
            frmSobre oi = new frmSobre();
            oi.Show();
            this.Close();
        }

        private void btnOrdemServico_Click(object sender, EventArgs e)
        {

        }

        private void fabricantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmFabricante_consultar_excluir_alterar tela = new frmFabricante_consultar_excluir_alterar();
            tela.Show();
            this.Close();
        }

        private void logoffToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fazer logoff ?",
                                                 "Projeto Sigma",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                LoginUsuario tela = new LoginUsuario();
                tela.Show();
                this.Close();
            }
        }
    }
}
