﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Cadastro_de_funcionario;
using Tela_principal.Cadastro_Usuario;
using Tela_principal.Ordem_de_Servico;
using Tela_principal.Ordem_de_Servico.Status;

namespace Tela_principal.Sub_Telas
{
    public partial class frmordemdeservicoStatus : Form
    {
        public frmordemdeservicoStatus()
        {
            InitializeComponent();
            CarregarCombos();
        }

        public void CarregarCombos()
        {
            Business_Funcionario db = new Business_Funcionario();
            List<DTO_Funcionario> lista = db.Listar();

            cboStatus.ValueMember = nameof(DTO_Funcionario.ID);
            cboStatus.DisplayMember = nameof(DTO_Funcionario.Nome);

            cboStatus.DataSource = lista;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja realmente fechar este programa?",
                                              "Projeto Sigma",
                                              MessageBoxButtons.YesNo,
                                              MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DTO_Funcionario funcionario = cboStatus.SelectedItem as DTO_Funcionario;

            DTO_Status dto = new DTO_Status();
            dto.ID_Funcionario = funcionario.ID;
            dto.Descricao = rtxtDescricao.Text;
            dto.Data_Inicio = dtpInicio.Value;
            dto.Data_Fim = dtpFim.Value;
            dto.Hora = txtHora.Text;
            dto.Hora2 = txtTime.Text;
            dto.Kilometragem = txtKilometragem.Text;
            dto.Periodo = txtPeriodo.Text;
            dto.Laudo = rtxtLaudo.Text;
            if (rdnCancelado.Checked == true)
            {
                dto.Status = rdnCancelado.Checked;
            }
            else if (rdnConcluido.Checked == true)
            {
                dto.Status = rdnConcluido.Checked;
            }
            else
            {
                dto.Status = rdnEmAndamento.Checked;
            }

            Business_Status db = new Business_Status();
            db.Salvar(dto);

            MessageBox.Show("Salvo com Sucesso!", 
                            "Sigma",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
