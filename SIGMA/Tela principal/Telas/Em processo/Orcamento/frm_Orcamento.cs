﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tela_principal.Fim_Orcamento;
using Tela_principal.Orcamento;
using Tela_principal.Ordem_de_Servico;
using Tela_principal.Sub_Telas;

namespace Tela_principal.Orçamento
{
    public partial class frmOrcamento : Form
    {
        public frmOrcamento()
        {
            InitializeComponent();
           
        }


        void Calculos ()
        {
            /*Conta quantos registros tem na coluna
             Label recebe a quantidade de registros contados convertido em string 
             Após os dados aparecerem na gried cada linha que irá aparecer será contada , convertida e escrita*/
            lbltotalservico.Text = dgvterceira.Rows.Count.ToString();
            lbltotalpecas.Text = dgvquarta.Rows.Count.ToString();

            //Descontos
            int peca = Convert.ToInt32(lbltotalpecas.Text) ;
            double pecadec = peca * 0.10;
            lbldecpeca.Text = pecadec.ToString();

            int mao = Convert.ToInt32(lbltotalservico.Text);
            double maodec = mao * 0.5;
            lbldecmao.Text = maodec.ToString();
            //Labels do canto superior 

            //Placa
            VIEW_Servico vw = new VIEW_Servico();
            lblplaca.Text = vw.Placa;

            //sub valor do arçamento
            double soma = 0;

            for (int i = 0; i < dgvquarta.Rows.Count; ++i)
            {

                soma += Convert.ToInt32(dgvquarta.Rows[i].Cells[2].Value);

            }

            lblsub.Text = soma.ToString();
            //Valor do orçamento 

            double valor1 = peca - pecadec;
            double valor2 = mao - maodec;
            double total = valor1 + valor2 + soma;

            lblvalororcamento.Text = total.ToString();

        }
        void CarregarPrimeiraGrid()
        {
           Database_Orcamento db = new Database_Orcamento();
           List<VIEW_Fim_orcamento> lista = db.Listar();

           dgvprimeiro.AutoGenerateColumns = false;
           dgvprimeiro.DataSource = lista;
        }

        void CarregarSegundaGrid()
        { 
            Database_Orcamento db = new Database_Orcamento();
            List<VIEW_Servico> lista = db.Servico();
            dgvsegundo.AutoGenerateColumns = false;
            dgvsegundo.DataSource = lista;

            
        }

        void CarregarTerceiraGrid ()
        {
            Database_Orcamento db = new Database_Orcamento();
            List<DTO_Pecas> lista = db.Pecas();

            dgvterceira.AutoGenerateColumns = false;
            dgvterceira.DataSource = lista;
        }


        void CarregarQuartaGrid()
        {
            Database_Orcamento db = new Database_Orcamento();
            List<VIEW_Servico> lista = db.Servico();

            

            dgvquarta.AutoGenerateColumns = false;
            dgvquarta.DataSource = lista;



            //está contando aparentemente
            //int cont = 0;
            //string aux = "";

            //foreach (DataGridViewRow row in dgvquarta.SelectedRows)
            //{

            //    string servico = dgvquarta.CurrentRow.Cells[1].Value.ToString();

            //    if (servico == aux)
            //    {
            //        cont += cont;
            //    }
            //    aux = servico;
            //}

            

        }



        private void lblFechar_Click(object sender, EventArgs e)
        {
                this.Hide();
                frmTela_Principal tela = new frmTela_Principal();
                tela.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastrar_Fabricantes tela = new frmCadastrar_Fabricantes();
            tela.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtcodigo.Text == string.Empty 
                && txtmodelodoveiculo.Text == string.Empty 
                && txtnomedoservico.Text == string.Empty) {
                //Busca 
                CarregarPrimeiraGrid();
                CarregarTerceiraGrid();
                Calculos();
            }
            else
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTela_Principal tela = new frmTela_Principal();
            tela.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmOrdem_Servico tela = new frmOrdem_Servico();
            tela.Show();
        }
    }
}
